//
//  kmp_taskreschedule.cpp
//  libomp
//
//  Copyright © 2018 Jérôme Richard - Gautier Thierry. All rights reserved.
//
#include "kmp_config.h"
#include "kmp.h"
#include "kmp_i18n.h"


#include "kmp_taskreschedule.h"

/*
*/
kaapi_wsqueue_t* kaapi_begin_graph(kmp_info_t *thread, uint32_t flag)
{
#if OMP_40_ENABLED && LIBOMP_USE_REORDER4LOCALITY
  kmp_task_team_t*       task_team = thread->th.th_task_team;
  if (task_team ==0) return 0;
  kaapi_wsqueue_t* ws;
#if USE_FAST_MEMORY
  ws = (kaapi_wsqueue_t *)__kmp_fast_allocate(thread, sizeof(kaapi_wsqueue_t));
#else
  ws = (kaapi_wsqueue_t *)__kmp_thread_malloc(thread, sizeof(kaapi_wsqueue_t));
#endif
  kaapi_wsqueue_init( ws, INITIAL_TASK_DEQUE_SIZE, -1 );
  thread->th.th_tasklist = ws;
  return ws;
#else
  return 0;
#endif
}


/*
*/
void kaapi_push_task( kmp_info_t* thread, kaapi_task_t* task )
{
#if OMP_40_ENABLED && LIBOMP_USE_REORDER4LOCALITY
  kaapi_wsqueue_t* queue = thread->th.th_tasklist;
  task->next = 0;
  task->prev = queue->deque_T;
  if (task->prev ==0)
    queue->deque_H = task;
  else
    queue->deque_T->next = task;
  queue->deque_T = task;
  ++queue->deque_size;

  task->groupBegin = task;
  task->groupEnd = task;
#endif
}


/*
*/
void kaapi_end_graph(kmp_info_t *thread, uint32_t flag)
{
#if OMP_40_ENABLED && LIBOMP_USE_REORDER4LOCALITY
  if (thread->th.th_tasklist ==0) return;
  /* select the queue */
  kmp_task_team_t*       task_team = thread->th.th_task_team;
  kmp_int32              tid = thread->th.th_info.ds.ds_tid;
  kmp_base_queue_data_t *selected_queue = NULL;

#if LIBOMP_USE_AFFINITY
  int cpu = sched_getcpu();
  int selected_nodeid = __kmp_cpu2node(cpu);

  selected_queue = &task_team->tt.tt_task_private_queues[KMP_LEVEL_NUMA][selected_nodeid].qd;
#else
  selected_queue = &task_team->tt.tt_task_queues[KMP_LEVEL_THREAD][tid].qd;
#endif
  kaapi_wsqueue_t* queue = &(selected_queue->td_wsdeque);
  kaapi_wsqueue_t* tasklist = thread->th.th_tasklist;
  thread->th.th_tasklist = 0;
  kaapi_wsqueue_push_tasklist( queue, tasklist );
  KMP_ASSERT( kaapi_wsqueue_empty(tasklist) );
#if USE_FAST_MEMORY
  __kmp_fast_free(thread, tasklist);
#else /* ! USE_FAST_MEMORY */
  __kmp_thread_free(thread, tasklist);
#endif
#endif
}


/*
*/
void kaapi_reorder4locality_addDependency(
    kaapi_wsqueue_t* ready_queue,
    kaapi_task_t* predTask,
    kaapi_task_t* currtask,
    const kmp_depend_info_t* depinfo)
{
#if OMP_40_ENABLED && LIBOMP_USE_REORDER4LOCALITY
  // First dependency
  if(currtask->groupBegin == 0)
  {
    currtask->groupBegin = predTask->groupBegin;
    currtask->groupEnd = predTask->groupEnd;
  }
  else
  {
    // Group of predecessor are not contiguous
    // Assumption: submitted tasks are appended at the end of the ready list and dependencies are sorted
    // Assumption: groups are not aliasing (to check: not always true but seems to be ok)
    if(currtask->groupEnd->next != predTask->groupBegin)
    {
      // FAIL: group aliasing (with 3 levels) or replicated dependency

      // Hack to avoid handling the same task again when it comes to handle multiple dependencies
      if(currtask->groupBegin != predTask->groupBegin)
      {
        // Avoid partially the reordering of predTask antecedents if they are shared with those of currtask
        // TODO: support the general case
        if(currtask->groupEnd != predTask->groupEnd)
        {
          kaapi_task_t* itInsert = currtask->groupEnd;
          kaapi_task_t* itBegin = predTask->groupBegin;
          kaapi_task_t* itEnd = predTask->groupEnd;
          bool reorder = true;

          // In some case the algorithm fail to keep the coherence of data structure such as group markers
          // Iterating over the whole ready list is sufficient to check if something go wrong
          for(kaapi_task_t* it=itBegin ; it!=itEnd ; it=it->next)
          {
            if(it == NULL || it == itInsert)
            {
              reorder = false;
              break;
            }
          }

          // Improve locality regarding submission order by moving the predecessor group near the last one
          if(reorder)
            kaapi_wsqueue_splice( ready_queue, itInsert, itBegin, itEnd);
        }
      }
    }

    currtask->groupEnd = predTask->groupEnd;
  }
#endif
}

