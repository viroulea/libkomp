/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include "ompt-internal.h"
#include "omp.h"
#include "kmp.h"

#if LIBOMP_USE_NUMA
#ifndef _GNU_SOURCE
#    define _GNU_SOURCE
#endif
#include <sched.h> /* sched_getcpu */
#endif

/* used to only print event */
#define USE_KAAPI 1

#if USE_KAAPI
#include "kaapi_trace.h"
#include "kaapi_error.h"
#endif

#define LOG 0

#ifdef  __cplusplus
extern "C" {
#endif

static ompt_get_task_id_t ompt_get_task_id;
static ompt_get_thread_id_t ompt_get_thread_id;
static ompt_get_parallel_id_t ompt_get_parallel_id;


/* Return human readable name from encoding of the psource information */
/* psource: Input format: ";../../plasma/src/timing.inc;run;129;15;;"
   name == task name, should replace the function name
   Output format: "func: <funcname>\nfile: <filename>\nline: <linenumber>"
*/
static char* libomp_filter_func(char* output, int size, const char* psource, const char* name);



#if USE_KAAPI
#define KAAPI_MAX_NESTEDPARALLEL_TEAM 8

/* ------------ basic type to store information */
typedef struct {
  kaapi_descrformat_t* fdescr;                    /* of the top stack ! */
  void*                task;                      /* task on the top stack ! */
  kaapi_perf_counter_t accum[KAAPI_PERF_ID_MAX];  /* each of size eventset at most 64 */
} kaapi_taskstackentry_t;

typedef struct {
  int top;
  int capacity;
  kaapi_taskstackentry_t* stack;
  kaapi_perf_counter_t t0_task[KAAPI_PERF_ID_MAX];/* starting values of perfctr for the current task */
  kaapi_perf_counter_t t0[KAAPI_PERF_ID_MAX];     /* starting values of perfctr for the thread */
} kaapi_stack2ompt_t;

typedef struct {
  int top;
  kaapi_tracelib_team_t* stack[KAAPI_MAX_NESTEDPARALLEL_TEAM];/* starting values of perfctr for the current task */
} kaapi_stack2team_t;

/* In this version assume to ne recopy or extend array of such data type entry */
typedef struct {
  kaapi_tracelib_thread_t*    kproc;
  kaapi_stack2ompt_t          pstack;
  kaapi_stack2team_t          tstack;
} kaapi_ompt_thread_info_t __attribute__((aligned(64)));


static kmp_lock_t                __kaapi_global_lock;
static kaapi_ompt_thread_info_t* __kaapi_oth_info = 0;
static size_t                    __kaapi_oth_info_capacity = 256;

/*
*/
static inline void realloc_ifrequired( size_t size )
{
#if 0
  kmp_uint32 gtid = __kmp_entry_gtid();
  __kmp_acquire_lock(&__kaapi_global_lock, gtid);
  if (__kaapi_oth_info_capacity <size)
  {
    size_t old_size = __kaapi_oth_info_capacity;
    if (__kaapi_oth_info_capacity == 0) __kaapi_oth_info_capacity = 1;
    while (__kaapi_oth_info_capacity <size)
      __kaapi_oth_info_capacity *= 2;

    kaapi_ompt_thread_info_t* new_oth_info
        = (kaapi_ompt_thread_info_t*)realloc(__kaapi_oth_info, __kaapi_oth_info_capacity*sizeof(kaapi_ompt_thread_info_t) );
    for (int i=0; i< old_size; ++i)
      new_oth_info[i].stack = __kaapi_oth_info[i].stack;
    memset(&new_oth_info[old_size], 0, __kaapi_oth_info_capacity - old_size);
    __kaapi_oth_info = new_oth_info;
  }
  __kmp_release_lock(&__kaapi_global_lock, gtid);
#else
  exit(0);
#endif
}
#endif

/*
*/
static void
on_ompt_event_thread_begin(
    ompt_thread_id_t thread_id,
    ompt_thread_type_t thread_type
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_begin: thread_type=%" PRIu64 "\n", thread_id, (uint64_t)thread_type);
#endif
  kaapi_assert ( thread_id < __kaapi_oth_info_capacity );
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
#if LIBOMP_USE_NUMA
  int cpu = sched_getcpu();
#endif
  koti->kproc = kaapi_tracelib_thread_init(
    thread_id-1,
#if LIBOMP_USE_NUMA
    cpu,
    __kmp_cpu2node(cpu), /* ok because NUMA == linux. Note that information is incorrect; LibOMP move thread
at the begining of the parallel region */
#else
    0, 0,
#endif
    0
  );
  koti->pstack.top = 0;
  koti->pstack.capacity = 256;
  koti->pstack.stack = (kaapi_taskstackentry_t*)malloc(
    sizeof(kaapi_taskstackentry_t) * koti->pstack.capacity /*  */
  );
  koti->tstack.top = 0;
#if USE_KAAPI
  kaapi_tracelib_thread_start( koti->kproc );
#endif
}

/*
*/
static void
on_ompt_event_thread_end(
    ompt_thread_id_t thread_id,
    ompt_thread_type_t thread_type
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_end: thread_type=%" PRIu64 "\n", thread_id, (uint64_t)thread_type);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_stop( koti->kproc );
  kaapi_tracelib_thread_fini( koti->kproc );
#endif
}


/*
*/
static void
on_ompt_event_parallel_begin(
  ompt_thread_id_t thread_id,
  ompt_task_id_t   parent_task_id,
  ompt_frame_t    *parent_task_frame,
  ompt_parallel_id_t parent_parallel_id,
  ompt_parallel_id_t parallel_id,
  uint32_t           requested_team_size,
  void              *parallel_function,
  ompt_invoker_t     invoker,
  ompt_ident_t*      loc)
{
#if LOG
  printf("%" PRIu64 ": omp threadid:%" PRIu64 ": ompt_event_parallel_begin: parent_task_id=%" PRIu64 ", parent_task_frame=%p, parent_id=%" PRIu64 ", parallel_id=%" PRIu64 ", requested_team_size=%" PRIu32 ", parallel_function=%p, invoker=%d, source=%s\n", thread_id, (uint64_t)omp_get_thread_num(),
      parent_task_id, parent_task_frame, parent_parallel_id,
      parallel_id, requested_team_size, parallel_function,
      invoker, loc == 0? "undefined" : loc->psource
  );
#endif

#if USE_KAAPI
  /* TODO here: the key is the way several instances of the same parallel region are collapsed
     to compute statistics. 
     Default is to take key equal to the parallel_function pointer.
     
     Each tracelib_team_t is a per thread data structure used to capture performance counter
     for a parallel region.
     The set of kaapi_tracelib_team_t that forms the OMP parallel region is computed at the
     terminaison of the library in order to report overhead at the end, after the computation.
     The relation ship between a team and its parent team is known only on the master team thread.
     Statistics are computed among the different instances at the terminaison of the library.
  */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  void* key = parallel_function;
#if LIBOMP_USE_NUMA
  /* correct numaid here because when thread_begin, affinity seems not good with libomp */
  koti->kproc->numaid = __kmp_cpu2node(sched_getcpu());
#endif
  kaapi_tracelib_team_t* parent_team = 0;
  if (koti->tstack.top !=0)
    parent_team = koti->tstack.stack[koti->tstack.top-1];
  kaapi_tracelib_team_t* team = kaapi_tracelib_team_init(
            koti->kproc,
            key,
            parent_team,
            (void* (*)())parallel_function,
            loc ? loc->psource : 0,
            0, /* an other possible name in place for the defaut name encoded in psource */
            libomp_filter_func
  );
  koti->tstack.stack[koti->tstack.top] = team;
  kaapi_tracelib_team_start(koti->kproc,
      team,
      koti->tstack.top >0 ?  koti->tstack.stack[koti->tstack.top-1]: 0,
      parallel_id
  );
  ++koti->tstack.top;
#endif
}

static void
on_ompt_event_implicit_task_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id);
  
/*
*/
static void
on_ompt_event_parallel_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id,
  ompt_invoker_t invoker)
{
#if USE_KAAPI
  /* end implicit task here */
  on_ompt_event_implicit_task_end( thread_id, parallel_id, task_id );
#endif
#if LOG
  printf("%" PRIu64 ": on_ompt_event_parallel_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  /* end of the parallel region */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  int idxtop = --koti->tstack.top;
  kaapi_tracelib_team_stop( koti->kproc,
      koti->tstack.stack[idxtop],
      idxtop >0 ?  koti->tstack.stack[idxtop-1]: 0,
      parallel_id
  );
  kaapi_tracelib_team_fini( koti->kproc, koti->tstack.stack[idxtop] );
#endif
}


/*
*/
static void
on_ompt_event_implicit_task_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_implicit_task_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  /* This is code for implicit task begin.
  */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_team_t* team = koti->tstack.stack[koti->tstack.top-1];
  kaapi_descrformat_t* fdescr = kaapi_tracelib_register_fmtdescr(
        1,
        team->key, /* same key as the team. Not implicit task ? why ? [TG] */
        team->name,
        0,
        0          /* no filter: team name should be already well formed */
  );
  int idxtop = koti->pstack.top;
  koti->pstack.stack[idxtop].fdescr = fdescr;
  koti->pstack.stack[idxtop].task   = (void*)task_id;

  kaapi_tracelib_task_begin(
    koti->kproc,
    (kaapi_task_id_t)task_id,
    fdescr->fmtid,
    0,
    0, 0, 0,
    0, 0,
    koti->pstack.t0,
    0
  );
  ++koti->pstack.top;
#endif
}

/*
*/
static void
on_ompt_event_implicit_task_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  /* But because the end of implicit task is not related to on_ompt_event_parallel_end.
  */
#if LOG
  printf("%" PRIu64 ": ompt_event_implicit_task_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  /* thread 0 ends the parallel region
     But because the end of implicit task is not related to
     the end of the parallel region, we define here the end of implicit task.
  */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  int idxtop = --koti->pstack.top;
  //kaapi_assert( idxtop == 0 );

  /* reset that accumulated into */
  memset( koti->pstack.stack[idxtop].accum, 0, sizeof(koti->pstack.stack[idxtop].accum));
  kaapi_tracelib_task_end(
    koti->kproc,
    koti->pstack.stack[idxtop].task,
    0, 0,
    koti->pstack.stack[idxtop].fdescr,
    koti->pstack.t0,
    koti->pstack.stack[idxtop].accum
  );
#endif
}


/* does not correspond to true idle state: may steal & execute tasks */
static void
on_ompt_event_idle_begin(
    ompt_thread_id_t thread_id )
{
#if LOG
  printf("%" PRIu64 ": ompt_event_idle_begin: \n", thread_id);
#endif
}

/* does not correspond to true idle state: may steal & execute tasks */
static void
on_ompt_event_idle_end(
    ompt_thread_id_t thread_id )
{
#if LOG
  printf("%" PRIu64 ": ompt_event_idle_end\n", thread_id);
#endif
}


static void
on_ompt_event_master_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_master_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_master_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_master_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}


static void
on_ompt_event_loop_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t parent_task_id,
  ompt_workshare_id_t workshare_id,
  ompt_ident_t* loc,
  void *workshare_function,
  int schedule, int64_t lb, int64_t ub, int64_t stride, uint64_t chunk, uint64_t count)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_loop_begin: parallel_id=%" PRIu64 ", parent_task_id=%" PRIu64
    ", workshare_id=%" PRIu64 ", loc: %s, workshare_function=%p, schedule: %i"
    " [%" PRIi64 ":%" PRIi64 ":%" PRIi64 "], chunk:%" PRIu64 ", count:%" PRIu64 "\n",
    thread_id-1, parallel_id, parent_task_id, workshare_id,
    loc ? loc->psource : " undefined",
    workshare_function, schedule, lb, stride, ub, chunk, count);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  //kaapi_tracelib_team_t* team = koti->tstack.stack[koti->tstack.top-1];
  kaapi_tracelib_loop_begin(koti->kproc, /*team,*/ workshare_id,
    loc == 0? "undefined" : loc->psource,
    schedule, lb, ub, stride, count);
#endif
}

static void
on_ompt_event_loop_dispatch(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id,
  ompt_workshare_id_t workshare_id,
  int64_t lb, int64_t ub, int64_t stride
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_loop_dispatch: parallel_id=%" PRIu64 ", parent_task_id=%" PRIu64
    ", workshare_id=%" PRIu64 ", [%" PRIi64 ":%" PRIi64 ":%" PRIi64 "]\n",
    thread_id-1, parallel_id, task_id, workshare_id, lb, stride, ub);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_loop_dispatch( koti->kproc, workshare_id, ub, lb, stride);
#endif
}


static void
on_ompt_event_loop_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id,
  ompt_workshare_id_t workshare_id
  )
{
#if LOG
  printf("%" PRIu64 ": ompt_event_loop_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64
    ", workshare_id=%" PRIu64 "\n", thread_id-1, parallel_id, task_id, workshare_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_loop_end( koti->kproc, workshare_id );
#endif
}


static void
on_ompt_event_barrier_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_barrier_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_barrier_begin( koti->kproc, task_id );
#endif
}

static void
on_ompt_event_barrier_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_barrier_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_barrier_end( koti->kproc, task_id );
#endif
}

static void
on_ompt_event_wait_barrier_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_barrier_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_wait_barrier_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_barrier_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}


static void
on_ompt_event_taskwait_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_taskwait_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_taskwait_begin( koti->kproc );
#endif
}

static void
on_ompt_event_taskwait_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_taskwait_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_taskwait_end( koti->kproc );
#endif
}

static void
on_ompt_event_wait_taskwait_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_taskwait_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_wait_taskwait_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_taskwait_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_wait_taskgroup_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_taskgroup_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_wait_taskgroup_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_wait_taskgroup_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}

static void
on_ompt_event_taskgroup_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_taskgroup_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_switchstate(koti->kproc);
#endif
}

static void
on_ompt_event_taskgroup_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_taskgroup_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_switchstate(koti->kproc);
#endif
}

static void
on_ompt_event_initial_task_begin(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_initial_task_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}


static void
on_ompt_event_initial_task_end(
  ompt_thread_id_t thread_id,
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_initial_task_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_id, task_id);
#endif
}


static void
on_ompt_event_task_begin(
  ompt_thread_id_t thread_id,
  ompt_task_id_t parent_task_id,     /* id of parent task            */
  ompt_frame_t  *parent_task_frame,  /* frame data for parent task   */
  ompt_task_id_t new_task_id,        /* id of created task           */
  void *task_function,               /* pointer to outlined function */
  ompt_ident_t* loc,
  uint8_t  aff_kind,
  uint8_t  aff_strict,
  uint64_t aff_tag,
  uint8_t  rsrc,
  uint64_t* size_rsrc,
  const char* taskname
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_begin: parent_id=%" PRIu64 ", task_id=%" PRIu64 ", aff_kind=%i strict=%i tag=%" PRIu64 ", source= %s,\n", 
     thread_id, parent_task_id, new_task_id, aff_kind, aff_strict, aff_tag, loc == 0 ? "<undef>" : loc->psource );
#endif
#if USE_KAAPI
  /* This is code for implicit task begin.
  */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_switchstate(koti->kproc);
  kaapi_descrformat_t* fdescr = kaapi_tracelib_register_fmtdescr(
        0,
        task_function,
        loc ? loc->psource : 0,
        taskname,
        libomp_filter_func
  );
  int idxtop = koti->pstack.top;
  koti->pstack.stack[idxtop].fdescr = fdescr;
  koti->pstack.stack[idxtop].task   = (void*)new_task_id;

  kaapi_tracelib_task_begin(
    koti->kproc,
    (kaapi_task_id_t)new_task_id,
    fdescr->fmtid,
    1,
    aff_kind, aff_strict, aff_tag,
    rsrc, size_rsrc,
    koti->pstack.t0,
    koti->pstack.top ==0 ? 0 : koti->pstack.stack[idxtop-1].accum
  );
  ++koti->pstack.top;
#endif
}

static void
on_ompt_event_task_end(
  ompt_thread_id_t thread_id,
  ompt_task_id_t task_id,
  uint8_t  rsrc,
  uint64_t* size_rsrc
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_end: task_id=%" PRIu64 "\n", thread_id, task_id );
#endif
#if USE_KAAPI
  /* thread 0 ends the parallel region
     But because the end of implicit task is not related to
     the end of the parallel region, we define here the end of implicit task.
  */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  int idxtop = --koti->pstack.top;

  /* reset that accumulated into */
  memset( koti->pstack.stack[idxtop].accum, 0, sizeof(koti->pstack.stack[idxtop].accum));
  kaapi_tracelib_task_end(
    koti->kproc,
    koti->pstack.stack[idxtop].task,
    rsrc,
    size_rsrc,
    koti->pstack.stack[idxtop].fdescr,
    koti->pstack.t0,
    koti->pstack.stack[idxtop].accum
  );
  kaapi_tracelib_thread_switchstate(koti->kproc);
#endif
}

static void
on_ompt_event_task_switch(
  ompt_thread_id_t thread_id,
  ompt_task_id_t first_task_id,
  ompt_task_id_t second_task_id
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_switch: suspend_id=%" PRIu64 ", resume_id=%" PRIu64 "\n", thread_id, first_task_id, second_task_id);
#endif
}


#if USE_KAAPI
static void ompt_decoder( ompt_task_dependence_t* dep, int i, void** addr, size_t* len, int* mode /*, size_t* len */ )
{
  *addr = dep[i].variable_addr;
  *len  = dep[i].variable_len;
  *mode = KAAPI_ACCESS_MODE_VOID;
  if (dep[i].dependence_flags.in)
    *mode |=KAAPI_ACCESS_MODE_R;
  if (dep[i].dependence_flags.out)
    *mode |=KAAPI_ACCESS_MODE_W;
  if (dep[i].dependence_flags.cw)
    *mode |=KAAPI_ACCESS_MODE_CW;
  if (dep[i].dependence_flags.commute)
    *mode =KAAPI_ACCESS_MODE_R|KAAPI_ACCESS_MODE_W;
}
#endif

static void
on_ompt_event_task_dependences(
  ompt_thread_id_t thread_id,
  ompt_task_id_t task_id,            /* ID of task with dependences */
  const ompt_task_dependence_t *deps,/* vector of task dependences  */
  int ndeps,                         /* number of dependences       */
  const ompt_task_dependence_t *deps_noalias,/* vector of task dependences  */
  int ndeps_noalias                          /* number of dependences       */
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_dependences: tsak_id=%" PRIu64 ", #deps=%i\n", thread_id, task_id, ndeps);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_task_access(
      koti->kproc,
      (kaapi_task_id_t)task_id,
      ndeps,
      (void*)deps,
      ndeps_noalias,
      (void*)deps_noalias,
      (void (*)(void*, int, void**, size_t*, int*))ompt_decoder
  );
#endif
}


static void
on_ompt_event_task_dependence_pair(
  ompt_thread_id_t thread_id,
  ompt_task_id_t first_task_id,
  ompt_task_id_t second_task_id
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_dependence_pair: first_id=%" PRIu64 ", second_id=%" PRIu64 "\n", thread_id, first_task_id, second_task_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_task_depend(
      koti->kproc,
      (kaapi_task_id_t)first_task_id,
      (kaapi_task_id_t)second_task_id
  );
#endif
}


static void
on_ompt_event_task_attr(
  ompt_thread_id_t thread_id,
  ompt_task_id_t task_id,
  int kind,
  uint64_t value
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_task_attr: kind=%i, value=%" PRIu64 "\n", thread_id, kind, value);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_task_attr(
      koti->kproc,
      task_id != 0 ? (kaapi_task_id_t)task_id : koti->pstack.stack[koti->pstack.top-1].task,
      kind,
      value
  );
#endif
}


static void
on_ompt_event_thread_schedinfo(
  ompt_thread_id_t thread_id,
  uint64_t state,
  uint32_t cpu,
  uint32_t node
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_schedinfo: state id=%" PRIu64 ", cpu=%i, node=%i\n", thread_id, state, cpu, node);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_state( koti->kproc, 0, cpu, node, state );
#endif
}


static void
on_ompt_event_thread_state_begin(
  ompt_thread_id_t thread_id,
  uint64_t state,
  uint32_t cpu,
  uint32_t node
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_state_begin: state id=%" PRIu64 ", cpu=%i, node=%i\n", thread_id, state, cpu, node);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_state( koti->kproc, 0, cpu, node, state );
#endif
}

static void
on_ompt_event_thread_state_end(
  ompt_thread_id_t thread_id,
  uint64_t state,
  uint32_t cpu,
  uint32_t node
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_state_end: state id=%" PRIu32 "\n", thread_id, state);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_state( koti->kproc, 1, cpu, node, state );
#endif
}

static void
on_ompt_event_thread_steal(
  ompt_thread_id_t thread_id,
    int32_t        op,
    uint32_t       cpu,
    uint32_t       node,
    int32_t        level,
    int32_t        level_id
)
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_steal: op=%i, level=%i id=%i\n", thread_id, op, level, level_id);
#endif
#if USE_KAAPI
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  kaapi_tracelib_thread_stealop( koti->kproc, op, cpu, node, level, level_id );
#endif
}


/* 
*/
void kaapi_ompt_initialize(
  ompt_function_lookup_t lookup,
  const char *runtime_version,
  unsigned int ompt_version)
{
  /* TODO:
     - activate callback if and only if KAAPI_RECORD_MASK have some specific bits set to one
     -
  */
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_get_task_id = (ompt_get_task_id_t) lookup("ompt_get_task_id");
  ompt_get_thread_id = (ompt_get_thread_id_t) lookup("ompt_get_thread_id");
  ompt_get_parallel_id = (ompt_get_parallel_id_t) lookup("ompt_get_parallel_id");


  ompt_set_callback(ompt_event_thread_begin, (ompt_callback_t) &on_ompt_event_thread_begin);
  ompt_set_callback(ompt_event_thread_end, (ompt_callback_t) &on_ompt_event_thread_end);
  ompt_set_callback(ompt_event_parallel_begin, (ompt_callback_t) &on_ompt_event_parallel_begin);
  ompt_set_callback(ompt_event_parallel_end, (ompt_callback_t) &on_ompt_event_parallel_end);


  ompt_set_callback(ompt_event_barrier_begin, (ompt_callback_t) &on_ompt_event_barrier_begin);
  ompt_set_callback(ompt_event_barrier_end, (ompt_callback_t) &on_ompt_event_barrier_end);
  //ompt_set_callback(ompt_event_wait_barrier_begin, (ompt_callback_t) &on_ompt_event_wait_barrier_begin);
  //ompt_set_callback(ompt_event_wait_barrier_end, (ompt_callback_t) &on_ompt_event_wait_barrier_end);

  //ompt_set_callback(ompt_event_initial_task_begin, (ompt_callback_t) &on_ompt_event_initial_task_begin);
  //ompt_set_callback(ompt_event_initial_task_end, (ompt_callback_t) &on_ompt_event_initial_task_end);

  ompt_set_callback(ompt_event_taskwait_begin, (ompt_callback_t) &on_ompt_event_taskwait_begin);
  ompt_set_callback(ompt_event_taskwait_end, (ompt_callback_t) &on_ompt_event_taskwait_end);
  //ompt_set_callback(ompt_event_wait_taskwait_begin, (ompt_callback_t) &on_ompt_event_wait_taskwait_begin);
  //ompt_set_callback(ompt_event_wait_taskwait_end, (ompt_callback_t) &on_ompt_event_wait_taskwait_end);

  ompt_set_callback(ompt_event_taskgroup_begin, (ompt_callback_t) &on_ompt_event_taskgroup_begin);
  ompt_set_callback(ompt_event_taskgroup_end, (ompt_callback_t) &on_ompt_event_taskgroup_end);
  //ompt_set_callback(ompt_event_wait_taskgroup_begin, (ompt_callback_t) &on_ompt_event_wait_taskgroup_begin);
  //ompt_set_callback(ompt_event_wait_taskgroup_end, (ompt_callback_t) &on_ompt_event_wait_taskgroup_end);

  ompt_set_callback(ompt_event_implicit_task_begin, (ompt_callback_t) &on_ompt_event_implicit_task_begin);
  //Not set: called by parallel_end ompt_set_callback(ompt_event_implicit_task_end, (ompt_callback_t) &on_ompt_event_implicit_task_end);
  ompt_set_callback(ompt_event_task_begin, (ompt_callback_t) &on_ompt_event_task_begin);
  ompt_set_callback(ompt_event_task_end, (ompt_callback_t) &on_ompt_event_task_end);
  //ompt_set_callback(ompt_event_task_switch, (ompt_callback_t) &on_ompt_event_task_switch);
  ompt_set_callback(ompt_event_task_dependences, (ompt_callback_t) &on_ompt_event_task_dependences);
  ompt_set_callback(ompt_event_task_dependence_pair, (ompt_callback_t) &on_ompt_event_task_dependence_pair);
  ompt_set_callback(ompt_event_task_attr, (ompt_callback_t) &on_ompt_event_task_attr);


  ompt_set_callback(ompt_event_loop_begin, (ompt_callback_t) &on_ompt_event_loop_begin);
  ompt_set_callback(ompt_event_loop_end, (ompt_callback_t) &on_ompt_event_loop_end);
  ompt_set_callback(ompt_event_loop_dispatch, (ompt_callback_t) &on_ompt_event_loop_dispatch);
  //ompt_set_callback(ompt_event_master_begin, (ompt_callback_t) &on_ompt_event_master_begin);
  //ompt_set_callback(ompt_event_master_end, (ompt_callback_t) &on_ompt_event_master_end);

  //ompt_set_callback(ompt_event_idle_begin, (ompt_callback_t) &on_ompt_event_idle_begin);
  //ompt_set_callback(ompt_event_idle_end, (ompt_callback_t) &on_ompt_event_idle_end);

  ompt_set_callback(ompt_event_thread_schedinfo, (ompt_callback_t) &on_ompt_event_thread_schedinfo);
  ompt_set_callback(ompt_event_thread_state_begin, (ompt_callback_t) &on_ompt_event_thread_state_begin);
  ompt_set_callback(ompt_event_thread_state_end, (ompt_callback_t) &on_ompt_event_thread_state_end);
  ompt_set_callback(ompt_event_thread_steal, (ompt_callback_t) &on_ompt_event_thread_steal);
}


/*
*/
static int kaapi_ompt_init = 0;
_OMP_EXTERN
__attribute__ (( weak ))
ompt_initialize_t ompt_tool()
{
  printf("[OMP-TRACE] ompt-trace ompt_tool initialized\n");
  kaapi_ompt_init = 1;
  return &kaapi_ompt_initialize;
}


/*
*/
_OMP_EXTERN
__attribute__ (( weak ))
int kaapi_ext_init(void)
{
  int err = 0;
#if USE_KAAPI
  if (kaapi_ompt_init)
  {
    __kmp_init_lock( &__kaapi_global_lock );
    __kaapi_oth_info = (kaapi_ompt_thread_info_t*)calloc(__kaapi_oth_info_capacity, sizeof(kaapi_ompt_thread_info_t));
    err = kaapi_tracelib_init(
      getpid()
    );
    if (err !=0)
      printf("[OMP-TRACE] kaapi tracing, init error:%i, version: %s\n", err, get_kaapi_version());
    else
      printf("[OMP-TRACE] kaapi tracing version: %s\n",get_kaapi_version());
  }
#else
  if (kaapi_ompt_init)
  {
    printf("[OMP-TRACE] kaapi tracing, version compiled without function implementations\n");
  }
#endif
  
  return err;
}


/*
*/
_OMP_EXTERN
__attribute__ (( weak ))
void kaapi_ext_fini(void)
{
  if (kaapi_ompt_init)
  {
#if USE_KAAPI
    kaapi_tracelib_fini();
#endif
    printf("[OMP-TRACE] kaapi tracing tool closed.\n");
  }
}


/* Return human readable name from encoding of the psource information */
/* Input format: ";../../plasma/src/timing.inc;run;129;15;;" 
   Output format: "func: <funcname>\nfile: <filename>\nline: <linenumber>"
*/
char* libomp_filter_func(char* output, int size, const char* psource, const char* name)
{
  char* buffer = output;
  const char* p0;
  const char* p1;
  --size;
  if (psource ==0) goto return_fast;
  if (size <=0) return 0;

  /* pathname starting position */
  p0 = psource+1;

  /* functionname start: else name is the function name */
  p1 = strstr( p0+1, ";");
  if (p1 ==0)
  {
return_fast:
    if (name)
      return strcpy( output, name );
    else
      return 0;
  }

  /* find the filename in the path */
  p0 = p1-1;
  while (p0 != psource)
  {
    --p0;
    if (*p0== '/') break;
    if (*p0== ';') break;
  }
  ++p0;

  /* recopy file: <function name > */
  if ((buffer+5 - output) >= size) return output;
  ++p1;
  strcpy(buffer,"func: ");
  buffer += 6;
  if (name)
  {
    while (*name != 0)
    {
      if ((buffer - output) >= size) return output;
      *buffer++ = *name++;
    }
  }
  for (; *p1 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    if (name)
      p1++;
    else
      *buffer++ = *p1++;
  }
  *buffer++ = '\n';
  ++p1;

  /* recopy the filename */
  if ((buffer+5 - output) >= size) return output;
  strcpy(buffer,"file: ");
  buffer += 6;
  for (; *p0 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    *buffer++ = *p0++;
  }
  *buffer++ = '\n';

  /* recopy line number */
  if ((buffer+5 - output) >= size) return output;
  strcpy(buffer,"line: ");
  buffer += 6;
  for (; *p1 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    *buffer++ = *p1++;
  }
  if ((buffer - output) >= size) return output;
  *buffer = 0;

  return output;
}

#ifdef  __cplusplus
}
#endif
