/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_PERF_H
#define _KAAPI_PERF_H 1

#include <stddef.h>
#include "kaapi_atomic.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* return a reference to the idp-th performance counter of the k-processor
   in the current set of counters 
*/
#define KAAPI_PERF_REG(perfproc, idp) ((perfproc)->perf_regs[(idp)])
#define KAAPI_PERFCTR_INCR(perfproc,id, value) KAAPI_PERF_REG(perfproc,id) += value
#define KAAPI_PERFCTR_MAX(perfproc,id, value) if (KAAPI_PERF_REG(perfproc,id) < value) \
                                              KAAPI_PERF_REG(perfproc,id)=value


/* Meta data about performance counter 
*/
typedef struct {
  const char*            name;         /* human readable */
  const char*            cmdlinename;  /* command line name */
  const char*            helpstring;
  int                    eventcode;
  int                    ns2s;         /* 1 iff need conversion when displayed */
  int                    opaccum;      /* 0 = add, 1=max */
  uint32_t               type;
  kaapi_perf_counter_t (*read)(void*);
  void*                  ctxt;
} kaapi_perfctr_info_t;

extern kaapi_perfctr_info_t kaapi_perfctr_info[];

/* type of counter */
#define KAAPI_PCTR_LIBRARY    0x1
#define KAAPI_PCTR_PAPI       0x2
#define KAAPI_PCTR_USER       0x3

/** Predefined value for KAAPI_DISPLAY_PERF
*/
typedef enum  kaapi_display_perf_value {
  KAAPI_NO_DISPLAY_PERF      = 0,  /* do not display performance counters */
  KAAPI_DISPLAY_PERF_FULL    = 1,  /* display all counters, for each threads */
  KAAPI_DISPLAY_PERF_RESUME  = 2,  /* display only cumulated counters per process */
  KAAPI_DISPLAY_PERF_GPLOT   = 3,  /* display only gplot output */
  KAAPI_DISPLAY_PERF_DISPLAY = 4,
  KAAPI_DISPLAY_PERF_FINAL   = 5   /* display cumulative at the end */
} kaapi_display_perf_value_t;

/* Global variable for the sublibrary tracelib
*/
typedef struct {
  int                  gid;
  int                  cpucount;
  int                  gpucount;
  int                  numaplacecount;
  uint64_t             eventmask;
  const char*          recordfilename;      /* prefix for record filenames */
  kaapi_perf_idset_t   perfctr_idset;       /* per thread events */
  kaapi_perf_idset_t   taskperfctr_idset;   /* per task events */
  kaapi_perf_idset_t   uncoreperfctr_idset; /* uncore events */
  uint64_t             uncore_period;       /* period (ns) to collect uncore event counters */
  int                  proc_event_count;    /* !=0 number of events in perfctr_idset */
  int                  task_event_count;    /* !=0 number of events in taskperfctr_idset */
  int                  papi_event_count;    /* !=0 if PAPI is configured and papi counters defined */
  kaapi_descrformat_t**fmt_list;            /* array of fdescr */
  int                  fmt_listsize;
  kaapi_display_perf_value_t display_perfcounter;
  kaapi_atomic_t            nthreads;
  kaapi_tracelib_thread_t** threads;

} kaapi_tracelib_param_t;
extern kaapi_tracelib_param_t kaapi_tracelib_param;


/** Header of the trace file. First block of each trace file.
    Very static size
*/
#define KAAPI_SIZE_PERFCTR_NAME 128
typedef struct kaapi_eventfile_header {
  int            version;
  int            minor_version;
  int            trace_version;
  uint32_t       kid;
  uint32_t       numaid;
  uint32_t       ptype;
  unsigned int   cpucount;
  unsigned int   gpucount;
  int            numacount;           /* numa count */
  char           event_date_unit[8];  /* unit for the clock used to take date of event */
  uint64_t       event_mask;
  char           package[128];
  uint64_t       perf_mask;
  uint64_t       task_perf_mask;
  uint64_t       uncore_perf_mask;
  uint64_t       uncore_perf_period;
  int            perfcounter_count;   /* (idmax & 0xFF) | (base  & 0xFF for papi << 8) | (base uncore & 0xFF << 16) */
  int            taskfmt_count;       /* number of task's formats */
  char           perfcounter_name[KAAPI_PERF_ID_MAX][128]; /* name for each perf counter */
  kaapi_fmttrace_def fmtdefs[KAAPI_FORMAT_MAX]; /* of size taskfmt_count */
} kaapi_eventfile_header_t;


/* Team data structure = parallel region information.
   It contains information about all instances of parallel region.
   Two instances represent the same parallel region if they have the same function entry point.
*/
typedef struct kaapi_teamlinked_t {
  void*                        key;         /* team id: system wide identifier */
  const char*                  name;        /* team id: system wide identifier */
  int                          nthreads;    /* number of threads: size of thread_teams */
  int                          nthcnt;      /* size of thread_stats */
  kaapi_tracelib_team_t*      *thread_teams;/* all the per thread team stats */
  kaapi_perfstat_t*           *thread_stats;/* stats per thread */
  kaapi_perfstat_t             self_stats;  /* agglomeration of threads'stats  */
  kaapi_perfstat_t             total_stats; /* agglomeration of self team and sub teams */
  struct kaapi_teamlinked_t*   parent;
  struct kaapi_teamlinked_t*   firstchild;
  struct kaapi_teamlinked_t*   lastchild;
  struct kaapi_teamlinked_t*   sibling;
  struct kaapi_teamlinked_t*   next;
} kaapi_teamlinked_t;

/* idset */
static inline
void kaapi_perf_idset_zero(kaapi_perf_idset_t* set)
{
  *set = 0;
}
static inline
void kaapi_perf_idset_add(kaapi_perf_idset_t* set, kaapi_perf_id_t id)
{
  *set |= KAAPI_PERF_ID_MASK(id);
}
static inline
void kaapi_perf_idset_addset(kaapi_perf_idset_t* set, const kaapi_perf_idset_t mask)
{
  *set |= mask;
}
static inline
int kaapi_perf_idset_clear(kaapi_perf_idset_t* set, kaapi_perf_id_t pcid)
{
  *set &= (kaapi_perf_idset_t)(~KAAPI_PERF_ID_MASK(pcid));
  return 0;
}

static inline
int kaapi_perf_idset_test(const kaapi_perf_idset_t* set, kaapi_perf_id_t pcid)
{
  return (*set & (kaapi_perf_idset_t)KAAPI_PERF_ID_MASK(pcid)) !=0;
}
static inline
int kaapi_perf_idset_test_mask(const kaapi_perf_idset_t* set, kaapi_perf_idset_t mask)
{
  return (*set & mask) !=0;
}
static inline
unsigned int kaapi_perf_idset_size(const kaapi_perf_idset_t set)
{
  return __builtin_popcountl( set );
}
static inline
unsigned int kaapi_perf_idset_empty(const kaapi_perf_idset_t* set)
{
  return *set == 0;
}

#if defined(__cplusplus)
}
#endif

#endif
