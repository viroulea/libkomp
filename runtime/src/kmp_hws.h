/*
 * kmp_hws.h -- header for hiearchical workstealing stuff
 */

#ifndef KMP_HWS_H
#define KMP_HWS_H

#include "kmp.h"

#if LIBOMP_USE_NUMA
#include <numa.h>
#endif

class MachineInfo {
  unsigned int places_count[KMP_LEVEL_MAX];

  int init_done = 0;

public:
  unsigned int *cpu2node;
  unsigned int numa_nodes_count;
  size_t dram_size;        /* size of the DRAM, cste value */
  size_t L3_size;          /* size of the L3, cste value */
  size_t* numa_node_size;  /* NUMA node size, cste value */

#if LIBOMP_USE_EXTSCHED_MEM
  kmp_bootstrap_lock_t lock_dram; // Lock for accessing dram limit etc
  size_t dram_limit;        /* by using sched memory scheduler */
  size_t dram_consumption;  /* by using user level task attribut */
  size_t dram_real_consumption;  /* by using user level memory info (alloc/free) */
  size_t* L3_limit;         /* by using sched memory scheduler */
  size_t* L3_consumption;   /* by using user level task attribut */
#endif

  MachineInfo();
  ~MachineInfo();

  void init();
};

extern MachineInfo machine_info;


#endif // KMP_HWS_H
