/*
 * kmp_queues.h -- header for hiearchical workstealing stuff
 */

#ifndef KMP_QUEUES_H
#define KMP_QUEUES_H

#include "kmp.h"

static inline int __kmp_queue_empty(kmp_queue_data_t *queue)
{
#if LIBOMP_USE_THEQUEUE || LIBOMP_USE_LINKED_DEQUEUE
    return kaapi_wsqueue_empty(&queue->qd.td_wsdeque);
#else
    return TCR_4(queue ->qd. td_deque_ntasks) == 0;
#endif
}

void __kmp_init_task_deque(kmp_queue_data_t *queue, int level, int level_id, int node);

void __kmp_alloc_task_deque(kmp_base_queue_data_t *queue, int node);
void __kmp_realloc_task_deque(kmp_base_queue_data_t *queue);
void __kmp_free_task_deque(kmp_base_queue_data_t *own_queue);


#endif // KMP_QUEUES_H
