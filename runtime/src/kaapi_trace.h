/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_PERFLIB_H
#define _KAAPI_PERFLIB_H 1

#include <stdint.h>
#include <stddef.h>
#include "hw_count.h" 

#if defined(__cplusplus)
extern "C" {
#endif

#if !defined(KAAPI_MAX_HWCOUNTERS)
#define KAAPI_MAX_HWCOUNTERS 4
#endif

#if !defined(KAAPI_CACHE_LINE)
#define KAAPI_CACHE_LINE 64
#endif

/* ========================================================================= */
/* Kaapi API of exported function for the Kaapi timing                       */
/* ========================================================================= */

/** kaapi_get_elapsedtime
    The function kaapi_get_elapsedtime() will return the elapsed time in second
    since an epoch.
    Default (generic) function is based on system clock (gettimeofday).
    Optimized function is based on clock_gettime.
*/
extern uint64_t kaapi_get_elapsedns(void);

/** kaapi_get_elapsedns
    The function kaapi_get_elapsedtime() will return the elapsed time since an epoch
    in nano second unit.
*/
extern double kaapi_get_elapsedtime(void);


/* ========================================================================= */
/* Kaapi performance counter                                                 */
/* ========================================================================= */
/** \ingroup PERF
    Performace counters.
    The set of counters is decomposed in two classes:
    - software counters 
    - hardware counters using PAPI support to read and accumulate them.
    Both classes are unified in the Kaapi runtime : selection of events to count
    are specified using KAAPI_PERF_ and KAAPI_TASKPERF_ env. vars.
    
    On specific points in the execution, the runtime read selected event counters and
    accumulated them per task, per thread and per team.
    Some general remarks: all times are in ns, excepted if specified otherwise

    TODO: 
    - constraints some of the counter to no be cumulated per task or per thread but per socket
    - define group to simplify analysis of some code
*/
#define KAAPI_PERF_ID_MAX          64  /* maximal number of performance counters */

#define KAAPI_PERF_ID_WORK          0  /* computation time (user) or schedule time (sys) = tidle */
#define KAAPI_PERF_ID_TIDLE         1  /* schedule time (sys) = tidle */
#define KAAPI_PERF_ID_TINF          2  /* critical path */
#define KAAPI_PERF_ID_PTIME         3  /* parallel computation time (user) */
#define KAAPI_PERF_ID_TASKSPAWN     4  /* count number of spawned tasks */
#define KAAPI_PERF_ID_TASKEXEC      5  /* count number of executed tasks */
#define KAAPI_PERF_ID_CONFLICTPOP   6  /*  */
#define KAAPI_PERF_ID_STEALREQOK    7  /* count number of successful steal requests */
#define KAAPI_PERF_ID_STEALREQ      8  /* count number of steal requests emitted */
#define KAAPI_PERF_ID_STEALOP       9  /* count number of steal operation to reply to requests */
#define KAAPI_PERF_ID_STEALIN       10 /* count number of receive steal requests */
#define KAAPI_PERF_ID_TASKSTEAL     11 /* number of stolen task */
#define KAAPI_PERF_ID_SYNCINST      12 /* number of sync instruction */

#define KAAPI_PERF_ID_TOT_ENERGY    13 /* global consumption J */
#define KAAPI_PERF_ID_TOT_WATT      14 /* global consumption E/s = Watt */

#define KAAPI_PERF_ID_FREE0         15 /* */
#define KAAPI_PERF_ID_FREE1         16 /* */
#define KAAPI_PERF_ID_FREE2         17 /* */

#define KAAPI_PERF_ID_CACHE_HIT	    18 /* GPU cache hit */
#define KAAPI_PERF_ID_CACHE_MISS    19 /* GPU cache miss */
#define KAAPI_PERF_ID_ITERATION     20 /* number of iterations a thread performs */
#define KAAPI_PERF_ID_DFGBUILD      21 /* time to compute task lists in ns */
#define KAAPI_PERF_ID_RDLISTEXEC    22 /* time to execute tasks using ready lists in ns */
#define KAAPI_PERF_ID_RDLISTINIT    23 /* time to push tasks using ready lists in ns */
#define KAAPI_PERF_ID_LOCAL_READ    24 /* software counter of number of read parameter local to a task */
#define KAAPI_PERF_ID_LOCAL_WRITE 	25 /* software counter of number of written parameter local to a task  */
#define KAAPI_PERF_ID_REMOTE_READ   26 /* idem for remote data */
#define KAAPI_PERF_ID_REMOTE_WRITE  27 /* idem for remote data */
#define KAAPI_PERF_ID_PARALLEL      28 /* #parallel region */
#define KAAPI_PERF_ID_LOCK          29 /* #lock */
#define KAAPI_PERF_ID_BARRIER       30 /* #call to barrier */
#define KAAPI_PERF_ID_TASKYIELD     31 /* #call to taskyield */

#define KAAPI_PERF_ID_ENDSOFTWARE   32 /* mark end of software counters */

#define KAAPI_MASK_PERF_ID_SOFTWARE ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1)
#define KAAPI_PERF_ID_MASK(ID) (1UL << (ID))

/* All bit set to 1 in eventset after position KAAPI_PERF_ID_PAPI_BASE are papi events
   The name of the counter at bit i, is papi_names[i-KAAPI_PERF_ID_PAPI_BASE].
   Predefined PAPI event counter: those events exist only if they are defined
   in KAAPI_PERF_EVENTS or KAAPI_TASKPERF_EVENTS
*/
#define KAAPI_PERF_ID_PAPI_BASE    (KAAPI_PERF_ID_ENDSOFTWARE)


/* Group of events
   Definition of groups to make easy the definition of event set to capture.
   Exact definitions are available using environment variable KAAPI_HELPME set to non 0 value.
*/
#define KAAPI_PERF_GROUP_TASK      0
#define KAAPI_PERF_GROUP_SCHED     1
#define KAAPI_PERF_GROUP_OMP       2
#define KAAPI_PERF_GROUP_NUMA      3
#define KAAPI_PERF_GROUP_DFGBUILD  4
#define KAAPI_PERF_GROUP_OFFLOAD   5

#if ((KAAPI_PERF_ID_ENDSOFTWARE+KAAPI_MAX_HWCOUNTERS) > KAAPI_PERF_ID_MAX)
#error "The maximal size of the peformance counters handled by Kaapi should be extended. Please contact the authors."
#endif


/* counter type
*/
typedef int64_t kaapi_perf_counter_t;

/* identifier of perf event
*/
typedef uint32_t kaapi_perf_id_t;

/* type of set of events
*/
typedef uint64_t kaapi_perf_idset_t;


/* all counters per thread
*/
typedef kaapi_perf_counter_t kaapi_thread_perfctr_t[KAAPI_PERF_ID_MAX];


/* Basic stat for one performance counter
*/
typedef struct kaapi_one_perfstat_t {
  kaapi_perf_counter_t min;    /* min of the counters */
  kaapi_perf_counter_t max;    /* max of the counters */
  kaapi_perf_counter_t sum;    /* sum of the counters */
  kaapi_perf_counter_t sum2;   /* sum of the square of the counters */
} kaapi_one_perfstat_t;

/* Structure of size the number of defined performance counters per threads
   Todo: make it small to have the real size of the number of defined counters
*/
typedef struct kaapi_perfstat_t {
  size_t               count;                          /* number of occurrences to compute stat */
  kaapi_one_perfstat_t counters[KAAPI_PERF_ID_MAX];
} kaapi_perfstat_t;


/* Named perf counters with accumulation per kproc
*/
typedef struct kaapi_named_perfctr {
  const char*                   name;

  /* represent cummulated counters for all tasks of this format */
  struct kaapi_perfstat_t        stats;

  /* represent cummulated counters for all tasks of this format */
  struct kaapi_perfstat_t**      kproc_stats;
} kaapi_named_perfctr_t;



/* ========================================================================= */
/* Kaapi events recorderd in file                                            */
/* ========================================================================= */
/* Version 3 is not upper compatible with version 2.
   Only reader with trace version upper than 3 may have chance to read trace file
   Version 5 is not uppercompatible with older version.
   Version 6 is not uppercompatible with older versions.
   Version 7 is not uppercompatible with older versions and correspond to version integrated 
   in libomp.
   Version 9 add loop information, memory event, perf counter.
*/
#define __KAAPI_TRACE_VERSION__     9

/** Definition of internal KAAPI events.
    Not that any extension or modification of the events must
    be reflected in the utility bin/kaapi_event_reader.

    TODO: reduce the size of event by encoding information in extra field: e.g. begin / end
    of state, or group events by features and then used extra fields for number of subevent
*/
#define KAAPI_EVT_KPROC_START        0     /* kproc begins, i0: processor type (0:CPU, 1:GPU) */
#define KAAPI_EVT_KPROC_STOP         1     /* kproc ends */
#define KAAPI_EVT_TASK_BEG           2     /* begin execution of tasks */
#define KAAPI_EVT_TASK_END           3     /* end execution of tasks, d0: task, d1: numaid */
#define KAAPI_EVT_TASK_SUCC          4     /* T0 has successor T1 */
#define KAAPI_EVT_TASK_ACCESS        5     /* d0: task, d1: mode, d2: pointer */
#define KAAPI_EVT_COMP_DAG           6     /* computing the dag, i0[0]=1 iff beg, =0 iff else. d1: key */
/*#define KAAPI_EVT_FREE0            7*/
#define KAAPI_EVT_UNDEFINED_0        8
#define KAAPI_EVT_UNDEFINED_1        9
#define KAAPI_EVT_SCHED_IDLE_BEG     10    /* begin when k-processor begins to be idle and try to steal */
#define KAAPI_EVT_SCHED_IDLE_END     11    /* end when k-processor starts to steal */
#define KAAPI_EVT_UNDEFINED_2        12
#define KAAPI_EVT_UNDEFINED_3        13
#define KAAPI_EVT_UNDEFINED_4        14
#define KAAPI_EVT_SCHED_SUSPWAIT_BEG 15    /* master thread wait */
#define KAAPI_EVT_SCHED_SUSPWAIT_END 16    /* master thread end wait */
#define KAAPI_EVT_REQUEST_BEG        17    /* when k-processor begin to process requests, data=victim.id */
#define KAAPI_EVT_REQUEST_END        18    /* when k-processor end to process requests */
#define KAAPI_EVT_STEAL_OP           19    /* when k-processor emit a steal request data=victimid, serial*/
#define KAAPI_EVT_STEAL_AGGR_BEG     20    /* when begin to be a combiner */
#define KAAPI_EVT_STEAL_AGGR_END     21    /* when begin to be a combiner */
#define KAAPI_EVT_OFFLOAD_HTOH_BEG   22 /* offload copy */
#define KAAPI_EVT_OFFLOAD_HTOH_END   23
#define KAAPI_EVT_OFFLOAD_HTOD_BEG   24 /* offload copy */
#define KAAPI_EVT_OFFLOAD_HTOD_END   25
#define KAAPI_EVT_OFFLOAD_DTOH_BEG   26 /* offload copy */
#define KAAPI_EVT_OFFLOAD_DTOH_END   27
#define KAAPI_EVT_OFFLOAD_DTOD_BEG   28 /* offload copy */
#define KAAPI_EVT_OFFLOAD_DTOD_END   29
#define KAAPI_EVT_OFFLOAD_KERNEL_BEG 30
#define KAAPI_EVT_OFFLOAD_KERNEL_END 31
#define KAAPI_EVT_PARALLEL           32 /* i0[] = 1 iff beg, = 0 iff end, d1: parallel_id */
/*#define KAAPI_EVT_FREE0            33*/
#define KAAPI_EVT_TASKWAIT           34 /* i0[] = 1 iff beg, = 0 iff end, d1: task_id */
/*#define KAAPI_EVT_FREE0            35*/
#define KAAPI_EVT_TASKGROUP          36
/*#define KAAPI_EVT_FREE0            37*/
#define KAAPI_EVT_PERFCOUNTER        38 /* format <perf id (0, 1, 2..)>, <value> */
#define KAAPI_EVT_TASK_PERFCOUNTER   39 /* d0=task; d1.i8[0..2]: perf counter id; d2, d3: values */
                                        /* several KAAPI_EVT_TASK_PERFCOUNTER may follow KAAPI_EVT_TASK_END */
#define KAAPI_EVT_LOCK_BEG           40
#define KAAPI_EVT_LOCK_END           41
#define KAAPI_EVT_YIELD              42    /* i0[] = 1 iff beg, = 0 iff end */
/*#define KAAPI_EVT_FREE0            43*/
#define KAAPI_EVT_BARRIER            44    /* d0: running task */
/*#define KAAPI_EVT_FREE0            45*/
#define KAAPI_EVT_TASK_STEAL         46    /* d0: executed task, d1: original task */
#define KAAPI_EVT_LOOP_BEGIN         47    /* d0: workshare id, d1: sched type, d2: iteration count */
#define KAAPI_EVT_LOOP_END           48    /* d0: workshare id */
#define KAAPI_EVT_LOOP_NEXT          49    /* d0: workshare id, d1: ub, d2: lb, d3: stride */

#define KAAPI_EVT_TASK_STEALOP       50    /* d0: op:0 pop, 1 steal, d1.i32[0]:cpu, d1.i32[1]:node, d2.i32[0]:victim_level, d2.i32[1]: victim_level_id  */
#define KAAPI_EVT_THREAD_STATE       51    /* user level state: d0: 0 begin / 1 end; d1: cpu+node, d2 state */

#define KAAPI_EVT_ENERGY_MACHINE     52
#define KAAPI_EVT_WATT_MACHINE       53

#define KAAPI_EVT_LOOP_MDATA         54    /* d0: workshare id, d1: ub, d2: lb, d3: stride. Follow KAAPI_EVT_LOOP_BEGIN */
#define KAAPI_EVT_TASK_ATTR          55    /* d0: task, d1.i32[0]: kind, d2: value */
#define KAAPI_EVT_PERF_UNCORE        56    /* d0.i8[0]: number of uncore events in the record, d0.i8[1]..i8[3]: ids of uncore events in perfset d1,d2,d3: event counters  */
#define KAAPI_EVT_LAST               57

/** Size of the event mask 
*/
typedef uint64_t kaapi_event_mask_type_t;

/** Heler for creating mask from an event
*/
#define KAAPI_EVT_MASK(eventno) \
  (((kaapi_event_mask_type_t)1) << (kaapi_event_mask_type_t)eventno)

/* The following set is always in the mask
*/
#define KAAPI_EVT_MASK_STARTUP \
    (  KAAPI_EVT_MASK(KAAPI_EVT_KPROC_START) \
     | KAAPI_EVT_MASK(KAAPI_EVT_KPROC_STOP) \
    )

#define KAAPI_EVT_MASK_COMPUTE \
    (  KAAPI_EVT_MASK(KAAPI_EVT_TASK_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_SUCC) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_ACCESS) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_STEAL) \
     | KAAPI_EVT_MASK(KAAPI_EVT_COMP_DAG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_END) \
    )
    
#define KAAPI_EVT_MASK_OMP \
    (  KAAPI_EVT_MASK(KAAPI_EVT_PARALLEL) \
     | KAAPI_EVT_MASK(KAAPI_EVT_BARRIER) \
     | KAAPI_EVT_MASK(KAAPI_EVT_YIELD) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKWAIT) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKGROUP) \
     | KAAPI_EVT_MASK(KAAPI_EVT_LOOP_BEGIN) \
     | KAAPI_EVT_MASK(KAAPI_EVT_LOOP_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_LOOP_NEXT) \
     | KAAPI_EVT_MASK(KAAPI_EVT_LOOP_MDATA) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_ATTR) \
     | KAAPI_EVT_MASK(KAAPI_EVT_COMP_DAG) \
    )

#define KAAPI_EVT_MASK_SCHED \
    (  KAAPI_EVT_MASK(KAAPI_EVT_SCHED_IDLE_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_IDLE_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_SUSPWAIT_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_SUSPWAIT_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_STEAL_OP) \
     | KAAPI_EVT_MASK(KAAPI_EVT_STEAL_AGGR_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_STEAL_AGGR_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_REQUEST_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_REQUEST_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_THREAD_STATE) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_STEALOP) \
    )

#define KAAPI_EVT_MASK_ENERGY \
    (  KAAPI_EVT_MASK(KAAPI_EVT_ENERGY_MACHINE) \
     | KAAPI_EVT_MASK(KAAPI_EVT_WATT_MACHINE) )

#define KAAPI_EVT_MASK_OFFLOAD \
    (  KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOH_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOH_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOD_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOD_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOH_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOH_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOD_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOD_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_END) \
    )

#if 1
#define KAAPI_EVT_MASK_PERFCOUNTER \
    (  KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER)\
     | KAAPI_EVT_MASK(KAAPI_EVT_PERF_UNCORE)\
    )
#else
#define KAAPI_EVT_MASK_PERFCOUNTER \
    (  KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER)\
     | KAAPI_EVT_MASK(KAAPI_EVT_PERF_UNCORE)\
    )
#endif

/** Mask of events = kaapi_tracelib_param.eventmask
    The mask is set at runtime to select events that will be registered
    to file.
    The bit i-th in the mask is 1 iff the event number i is registered.
    It means that not more than sizeof(kaapi_event_mask_type_t)*8 events 
    are available in Kaapi.
*/

/* the datation used for event */
static inline uint64_t kaapi_event_date(void)
{
#if defined(KAAPI_USE_GETTICK) && (defined(__i386__) || defined(__pentium__) || defined(__pentiumpro__) || defined(__i586__) || defined(__i686__) || defined(__k6__) || defined(__k7__) || defined(__x86_64__))
#  define KAAPI_GET_TICK(t) __asm__ volatile("rdtsc" : "=a" ((t).sub.low), "=d" ((t).sub.high))
  union tick_t
  {
    uint64_t tick;
    struct {
      uint32_t low;
      uint32_t high;
    }
    sub;
  };
  union tick_t t; 
  KAAPI_GET_TICK(t);
  return t.tick;
#else
  return kaapi_get_elapsedns();
#endif
}

/* info about timer, must be coherent with kaapi_event_date() */
static inline const char* kaapi_event_date_unit(void)
{
#if defined(KAAPI_USE_GETTICK) && (defined(__i386__) || defined(__pentium__) || defined(__pentiumpro__) || defined(__i586__) || defined(__i686__) || defined(__k6__) || defined(__k7__) || defined(__x86_64__))
  return "cycle";
#else
  return "ns";
#endif
}


/* Event data. at most 3 event data per event.
*/
typedef union {
    void*     p;
    uintptr_t i; 
    double    d;
    uint64_t  u;
    uint64_t  i64[1];
    uint32_t  i32[2];
    uint16_t  i16[4];
    uint8_t   i8[8];
} kaapi_event_data_t;


/* Event 
  - fixed sized data structure. To be extended if required.
*/
typedef struct kaapi_event_t {
  uint8_t     evtno;        /* event number */
  uint8_t     size;         /* size of event: not used, event should have variable size [futur] */
  uint16_t    kid;          /* kaapi processor identifier or device identifier or other identifier */
  uint64_t    date;         /* nano second */
  union {
    struct {
      kaapi_event_data_t d0;
      kaapi_event_data_t d1;
      kaapi_event_data_t d2;
      kaapi_event_data_t d3;
    } s;
    kaapi_event_data_t data[4];
  } u;
} kaapi_event_t;

#define KAAPI_EVENT_DATA(evt,i,f) (evt)->u.s.d##i.f

/* Buffer
*/
#define KAAPI_EVENT_BUFFER_SIZE 65520
typedef struct kaapi_event_buffer_t {
  int                      kid;      /* set when buffer is pushed to the flushimator */
  int                      ptype;    /* set when buffer is pushed to the flushimator */
  uint32_t                 pos;
  kaapi_event_t            buffer[KAAPI_EVENT_BUFFER_SIZE];
  struct kaapi_event_buffer_t* next;
} kaapi_event_buffer_t;


struct kaapi_tracelib_team_t;

/* Context per thread for tracing and measuring some performance counters
*/
typedef struct kaapi_tracelib_thread_t {
  uint64_t                 kid;
  int                      ptype;           /* processor type */
  int                      numaid;          /* thread binding's NUMA node id */
  int                      cpu;             /* thread binding's NUMA node id */
  int                      mode;            /* if telapse is for tcompute or tidle */
  uint64_t                 tstart;          /* starting time for mode */
  kaapi_perf_idset_t       perfset;
  kaapi_perf_idset_t       task_perfset;
  struct kaapi_tracelib_loop_t* loop_head;  /* loop head list */
  struct kaapi_tracelib_loop_t* loop_tail;  /* loop tail list */
  uint64_t                 event_mask;
  kaapi_thread_perfctr_t   perf_regs;
  kaapi_event_buffer_t*    eventbuffer;
  int                      papi_event_set;
  unsigned int	           papi_event_count;
  kaapi_perf_idset_t	   papi_event_mask;
  struct kaapi_tracelib_team_t* head;
} __attribute__((aligned (KAAPI_CACHE_LINE))) kaapi_tracelib_thread_t;


/* Team data structure = parallel region information.
   It contains information about all instances of parallel region.
   Two instances represent the same parallel region if they have the same function entry point.
*/
typedef struct kaapi_tracelib_team_t {
  void*                         key;         /* uniq id for the team. */
  char*                         name;        /* name of the parallel region */
  struct kaapi_tracelib_team_t* parent_team; /* parent team */
  kaapi_perfstat_t              stats;       /* for the running thread */
  kaapi_thread_perfctr_t        perf0;       /* start of // region */
  kaapi_thread_perfctr_t        perf;        /* accumulation of counters for the // region */
  struct kaapi_tracelib_team_t* next;        /* in the team declaration */
} kaapi_tracelib_team_t;


/* Team data structure = parallel region information.
   It contains information about all instances of parallel region.
   Two instances represent the same parallel region if they have the same function entry point.
*/
typedef struct kaapi_tracelib_loop_t {
  void*                         key;         /* uniq id for the loop. */
  char*                         name;        /* name of the parallel region */
  kaapi_perfstat_t              stats;       /* for the running thread */
  kaapi_perf_counter_t          iter;        /* iteration count for the running thread */
  kaapi_perf_counter_t          ub;          /* ub for the loop */
  kaapi_perf_counter_t          lb;          /* lb */
  kaapi_perf_counter_t          stride;      /* stride */
  kaapi_perf_counter_t          time;        /* time for perf iteration for the running thread */
  struct kaapi_tracelib_loop_t* next;        /* in the team declaration */
} kaapi_tracelib_loop_t;




/* ========================================================================= */
/* Shared object and access mode                                             */
/* ========================================================================= */
/** Kaapi access mode mask
    \ingroup DFG
*/
/*@{*/
typedef enum kaapi_access_mode_t {
  KAAPI_ACCESS_MODE_VOID= 0,        /* 0000 0000 : */
  KAAPI_ACCESS_MODE_V   = 1,        /* 0000 0001 : */
  KAAPI_ACCESS_MODE_R   = 2,        /* 0000 0010 : */
  KAAPI_ACCESS_MODE_W   = 4,        /* 0000 0100 : */
  KAAPI_ACCESS_MODE_CW  = 8,        /* 0000 1000 : */
  KAAPI_ACCESS_MODE_S   = 16,       /* 0001 0000 : stack data */
  KAAPI_ACCESS_MODE_T   = 32,       /* 0010 0000 : for Quark support: scratch mode or temporary */
  KAAPI_ACCESS_MODE_P   = 64,       /* 0100 0000 : */
  KAAPI_ACCESS_MODE_IP  = 128,      /* 1000 0000 : in place, for CW only */

  KAAPI_ACCESS_MODE_RW  = KAAPI_ACCESS_MODE_R|KAAPI_ACCESS_MODE_W,
  KAAPI_ACCESS_MODE_STACK = KAAPI_ACCESS_MODE_S|KAAPI_ACCESS_MODE_RW,
  KAAPI_ACCESS_MODE_SCRATCH = KAAPI_ACCESS_MODE_T|KAAPI_ACCESS_MODE_V,
  KAAPI_ACCESS_MODE_CWP = KAAPI_ACCESS_MODE_P|KAAPI_ACCESS_MODE_CW,
  KAAPI_ACCESS_MODE_ICW = KAAPI_ACCESS_MODE_IP|KAAPI_ACCESS_MODE_CW
} kaapi_access_mode_t;

#define KAAPI_ACCESS_MASK_RIGHT_MODE   0x7F   /* 5 bits, ie bit 0, 1, 2, 3, 4, including P mode */
#define KAAPI_ACCESS_MASK_MODE         0x3F   /* without P, IP mode */
#define KAAPI_ACCESS_MASK_MODE_P       0x80   /* only P mode */
/*@}*/

/** Kaapi macro on access mode
    \ingroup DFG
*/
/*@{*/
#define KAAPI_ACCESS_GET_MODE( m )     ((m) & KAAPI_ACCESS_MASK_MODE )
#define KAAPI_ACCESS_IS_READ( m )      ((m) & KAAPI_ACCESS_MODE_R)
#define KAAPI_ACCESS_IS_WRITE( m )     ((m) & KAAPI_ACCESS_MODE_W)
#define KAAPI_ACCESS_IS_CUMULWRITE( m )((m) & KAAPI_ACCESS_MODE_CW)
#define KAAPI_ACCESS_IS_STACK( m )     ((m) & KAAPI_ACCESS_MODE_S)
#define KAAPI_ACCESS_IS_POSTPONED( m ) ((m) & KAAPI_ACCESS_MASK_MODE_P)
#define KAAPI_ACCESS_IS_ONLYWRITE( m ) (KAAPI_ACCESS_IS_WRITE(m) && !KAAPI_ACCESS_IS_READ(m))
#define KAAPI_ACCESS_IS_READWRITE( m ) (KAAPI_ACCESS_IS_READ(m) && KAAPI_ACCESS_IS_WRITE(m))

/** Return true if two modes are concurrents
    a == b and a or b is R or CW
    or a or b is postponed.
*/
#define KAAPI_ACCESS_IS_CONCURRENT(a,b) \
    ((((a)==(b)) \
 && (((b) == KAAPI_ACCESS_MODE_R)||((b)==KAAPI_ACCESS_MODE_CW))) || ((a|b) & KAAPI_ACCESS_MODE_P))
/*@}*/


#ifndef KAAPI_FORMAT_MAX
#define KAAPI_FORMAT_MAX             128
#endif

/* 
*/
typedef struct kaapi_fmttrace_def {
  uint64_t          fmtid;       /* id */
  char              name[64];    /* name */
  char              color[32];   /* color to represent the task */
} kaapi_fmttrace_def;


/* Information about task in the trace file
*/
typedef struct kaapi_descrformat_t {
  uint64_t                    fmtid;
  uint8_t                     implicit; /* from open implicit task */
  const char*                 name;
  const char*                 color;
  struct kaapi_descrformat_t* parent;
  struct kaapi_descrformat_t* firstchild;
  struct kaapi_descrformat_t* lastchild;
  struct kaapi_descrformat_t* sibling;
  kaapi_named_perfctr_t*      perfctr;
} kaapi_descrformat_t;


/* ========================================================================= */
/* Public API of exported function for the Kaapi tracing library             */
/* ========================================================================= */

/* 
*/
const char* get_kaapi_version(void);


/* Initialize Kaapi sublibrary
   Initialize different masks from KAAPI_RECORD_MASK & KAAPI_RECORD_TRACE
   Initialize different masks from KAAPI_TASKPERF_EVENTS & KAAPI_PERF_EVENTS
*/
extern int kaapi_tracelib_init(
  int gid
);


/* Finalization 
*/
extern void kaapi_tracelib_fini(void);


/* Initialize trace & performance counters for the current thread
   A call to kaapi_tracelib_thread_init declares a performance counters set and
   an event stream. If the tracing sublibrary is initialized such that the performance
   counter set or the event set to capture are empty, then the corresponding
   performance counter set or the event stream is set to 0.
   Return 0 in case of error.
*/
extern kaapi_tracelib_thread_t* kaapi_tracelib_thread_init (
    uint64_t               kid,
    int                    cpu,
    int                    numaid,
    int                    proctype
);

/* Return the size of events defined to be captured.
*/
static inline unsigned int kaapi_tracelib_thread_idsetsize(
    const kaapi_tracelib_thread_t* kproc
)
{ return __builtin_popcountl( kproc->perfset ); }

/* Return the size of events defined to be captured.
*/
static inline unsigned int kaapi_tracelib_thread_taskidsetsize(
    const kaapi_tracelib_thread_t* kproc
)
{ return __builtin_popcountl( kproc->task_perfset ); }


/* Start counting for the current thread kproc 
*/
extern void kaapi_tracelib_thread_start (
    kaapi_tracelib_thread_t*     kproc
);

/* Stop counting for the current thread kproc
   Performance counters are stored in kproc->perf_regs
*/
extern void kaapi_tracelib_thread_stop (
    kaapi_tracelib_thread_t*     kproc
);


/* Read the current value of counters and report them in regs
   regs should have enough size to store all events defined in the kproc set 
*/
extern void kaapi_tracelib_thread_read(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_perf_idset_t           idset,
    kaapi_perf_counter_t*        regs
);


/* Switch to count time from tidle to twork
   Return the time take into accounting
*/
extern int kaapi_tracelib_thread_switchstate(
    kaapi_tracelib_thread_t*     kproc
);

/* Finalization for the thread performance counter set or event stream.
   After the call to kaapi_tracelib_thread_fini data pointer by perf or
   eventstream are not usable.
*/
extern void kaapi_tracelib_thread_fini (
    kaapi_tracelib_thread_t*     kproc
);


/* Generates event 'BEGIN|END_STATE' information for the current thread.
   Interpretation is free.
*/
extern void kaapi_tracelib_thread_state (
    kaapi_tracelib_thread_t*     kproc,
    uint8_t                      begend, /* 0==begin, 1==end */
    uint32_t                     cpu,
    uint32_t                     node,
    uint64_t                     state
);


/* Generates event 'STEAL' information for the current thread.
   Interpretation is free.
*/
extern void kaapi_tracelib_thread_stealop (
    kaapi_tracelib_thread_t*     kproc,
    uint8_t                      op,
    uint32_t                     cpu,
    uint32_t                     node,
    uint32_t                     victim_level,
    uint32_t                     victim_level_id
);


/* Create a new per team stat data structure.
   Parent team is mandatory and used to recover hierarchy of team.
   All parallel region with the same routine are mapped to the same
   team stat data structure.
   The name and the filter_name are used if the team is newly created.
*/
extern kaapi_tracelib_team_t* kaapi_tracelib_team_init(
  kaapi_tracelib_thread_t* kproc,
  void*                    key,
  kaapi_tracelib_team_t*   parent_team,
  void*                  (*routine)(),
  const char*              psource,
  const char*              name,
  char*                  (*filter_name)(char*, int, const char*, const char*)
);


/*
*/
extern int kaapi_tracelib_team_fini(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t*   team
);


/*
*/
extern void kaapi_tracelib_team_start(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t*   team,
    kaapi_tracelib_team_t*   parent,
    int                      parallel_id
);

/*
*/
extern void kaapi_tracelib_team_stop(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t*   team,
    kaapi_tracelib_team_t*   parent,
    int                      parallel_id
);

/*
*/
typedef void* kaapi_task_id_t;
extern kaapi_task_id_t kaapi_tracelib_newtask_id(void);

/* Start executing the task 'task' with specified format id.
   perfctr0 is the starting set of counters maintained by the running thread.
   The accounting for task is the following:
     - read performance counter in tmp
     - if parent_perfctr is non null then do parent_perfctr += tmp - perfctr0
     - store tmp in perfctr0. 
   On return perfctr0 is the starting set of counters for the task 'task'.
   While performance counters until the task begin where accumulated to the parent task.
*/
extern void kaapi_tracelib_task_begin(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    uint64_t                     fmtid,
    int8_t                       isexplicit,
    int8_t                       kind,
    int8_t                       strict,
    uint64_t                     tag,
    uint8_t                      rsrc,
    uint64_t*                    size_rsrc,
    kaapi_perf_counter_t*        perfctr0,
    kaapi_perf_counter_t*        parent_perfctr
);


/* End of execution of the task 'task'. Return computed Tinf
   accumulate in task_perfctr the values of the current counters minus
   the starting values of the counters.
   Ie. compute
     task_perfctr <- task_perfctr + (current - perfctr0)
   and accumulated value in named_perfctr for statitics.
   task_perfctr may be 0 and was not returned.
   On return, current counters are stored in perfctr0.
*/
extern void kaapi_tracelib_task_end(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    uint8_t                      rsrc,
    uint64_t*                    size_rsrc,
    kaapi_descrformat_t*         fdescr,
    kaapi_perf_counter_t*        perfctr0,
    kaapi_perf_counter_t*        task_perfctr
);

/* Returns the name of the performance counter id */
extern const char* kaapi_tracelib_perfid_to_name(kaapi_perf_id_t id);

/* create new user counter with function used to collect counter value */
extern kaapi_perf_id_t kaapi_tracelib_create_perfid(
      const char* name,
      kaapi_perf_counter_t (*read)(void*),
      void* ctxt
);

/* Reserve named' performance counters
*/
extern kaapi_descrformat_t* kaapi_tracelib_reserve_perfcounter( );

/* Register a new format descriptor if not already done and returns it.
   New created descriptor entry are serialized.
*/
kaapi_descrformat_t* kaapi_tracelib_register_fmtdescr(
      int implicit,
      void* key,
      const char* psource,
      const char* task_name,
      char* (*filter_func)(char*, int, const char*, const char*)
);


/* 
*/
extern void kaapi_tracelib_taskwait_begin(
    kaapi_tracelib_thread_t*     kproc
);

/* 
*/
extern void kaapi_tracelib_taskwait_end(
    kaapi_tracelib_thread_t*     kproc
);


/*
*/
extern void kaapi_tracelib_task_depend(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              source,
    kaapi_task_id_t              sink
);

/*
*/
extern void kaapi_tracelib_task_attr(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t task,
    int kind,
    int64_t value
);

/* Decoder should returns the address and the access mode of the i-th parameters stored in deps.
   The access mode corresponds to the Kaapi access mode KAAPI_ACCESS_XX defined above. 
*/
extern void kaapi_tracelib_task_access(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    int                          count,
    void*                        deps,
    int                          count_noalias,
    void*                        deps_noalias,
    void                       (*decoder)(void*, int, void**, size_t*, int*)
);


/* Thread's synchronization related functions
*/
extern void kaapi_tracelib_barrier_begin(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid
);

/* 
*/
extern void kaapi_tracelib_barrier_end(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid
);

/* Thread's loop related functions
*/
extern void kaapi_tracelib_loop_begin(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid,
    const char*                  psource,
    uint64_t                     schedtype,
    int64_t                      lb,
    int64_t                      ub,
    int64_t                      stride,
    uint64_t                     count
);

/* 
*/
extern void kaapi_tracelib_loop_end(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid
);

/* 
*/
extern void kaapi_tracelib_loop_dispatch(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid,
    int64_t                      lb,
    int64_t                      ub,
    int64_t                      stride
);


/* utility */
extern size_t kaapi_tracelib_count_perfctr(void);

/* Human readable name of events */
extern const char* kaapi_event_name[];



/** Initialize the event recorder sub library.
    Must be called before any other functions.

*/
void kaapi_eventrecorder_init();


/** Destroy the event recorder sub library.
*/
void kaapi_eventrecorder_fini(void);

/** Open a new buffer for kprocessor kid.
    Kid must be a valid kid.
    \param ptype the type (KAAP_PROC_TYPE_XXX) of kid
    \param kid the identifier of a kprocessor or a device. Must be unique between threads.
    \retval a new eventbuffer
*/
extern kaapi_event_buffer_t* kaapi_event_openbuffer(int kid, unsigned int ptype);


/** Flush the event buffer evb and return and new buffer.
    \param evb the event buffer to flush
    \retval the new event buffer to use for futur records.
*/
extern kaapi_event_buffer_t* kaapi_event_flushbuffer( kaapi_event_buffer_t* evb );

/** Flush the event buffer and close the associated file descriptor.
*/
extern void kaapi_event_closebuffer( kaapi_event_buffer_t* evb );


/** Fence: write all flushed buffers and return
*/
extern int kaapi_event_fencebuffers(void);

/** Return a new event into the eventbuffer of the kprocessor.
*/
static inline kaapi_event_t*  kaapi_event_get(
    kaapi_tracelib_thread_t*  kproc,
    uint64_t                  tclock,
    uint8_t                   eventno
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;
  return evt;
}

static inline kaapi_event_buffer_t* kaapi_event_push(
    kaapi_tracelib_thread_t*  kproc
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  evb->pos++;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t* kaapi_event_push0(
    kaapi_tracelib_thread_t*  kproc,
    uint64_t                  tclock,
    uint8_t                   eventno
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push1(
    kaapi_tracelib_thread_t*  kproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push2(
    kaapi_tracelib_thread_t*  kproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0,
    uint64_t                p1
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}


/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push3(
    kaapi_tracelib_thread_t*  kproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0,
    uint64_t                p1,
    uint64_t                p2
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;
  KAAPI_EVENT_DATA(evt,2,u) = p2;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}


/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push4(
    kaapi_tracelib_thread_t* kproc,
    uint64_t                 tclock,
    uint8_t                  eventno,
    uint64_t                 p0,
    uint64_t                 p1,
    uint64_t                 p2,
    uint64_t                 p3
)
{
  kaapi_event_buffer_t* evb = kproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  evt->evtno   = eventno;
  evt->kid     = kproc->kid;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;
  KAAPI_EVENT_DATA(evt,2,u) = p2;
  KAAPI_EVENT_DATA(evt,3,u) = p3;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = kproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}



/* Write event counter values in idset to the trace file
*/
extern kaapi_event_buffer_t* kaapi_event_push_perfctr(
    kaapi_tracelib_thread_t*    kproc,
    uint64_t                    date,
    uint8_t                     eventno,
    uint64_t                    d0,
    const kaapi_perf_idset_t*   idset,
    const kaapi_perf_counter_t* perfctr
);


/*
*/

#define KAAPI_IFUSE_TRACE(kproc,inst) \
    if ((kproc)->eventbuffer) { inst; }
#define KAAPI_EVENT_GET(kproc, kthread, eventno ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_get((kproc), kaapi_event_date(), eventno ) : 0 )
#define KAAPI_EVENT_PUSH(kproc, kthread, eventno ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push((kproc)) : 0 )

#define KAAPI_EVENT_PUSH0(kproc, kthread, eventno ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push0((kproc), kaapi_event_date(), eventno ) : 0 )
#define KAAPI_EVENT_PUSH1(kproc, kthread, eventno, p1 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push1((kproc), kaapi_event_date(), eventno, (uint64_t)(p1) ) : 0 )
#define KAAPI_EVENT_PUSH2(kproc, kthread, eventno, p1, p2 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push2((kproc), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2) ) : 0)
#define KAAPI_EVENT_PUSH3(kproc, kthread, eventno, p1, p2, p3 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push3((kproc), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3) ): 0)
#define KAAPI_EVENT_PUSH4(kproc, kthread, eventno, p1, p2, p3, p4 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push4((kproc), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3), (uint64_t)(p4) ): 0)
#define KAAPI_EVENT_PUSH_PERFCTR(kproc, kthread, pc, idset, perfctr ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER))) ? \
      kaapi_event_push_perfctr((kproc), kaapi_event_date(), KAAPI_EVT_TASK_PERFCOUNTER, (uintptr_t)pc, idset, perfctr ) : 0)
#define KAAPI_EVENT_PUSH_UNCORE_PERFCTR(kproc, kthread, numaid, idset, perfctr ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(KAAPI_EVT_PERF_UNCORE))) ? \
      kaapi_event_push_perfctr((kproc), kaapi_event_date(), KAAPI_EVT_PERF_UNCORE, (uintptr_t)numaid, idset, perfctr ) : 0)


/* push new event with given date (value return by kaapi_event_date()) 
   the macros returns the date of the event else 0
*/
#define KAAPI_EVENT_PUSH0_AT(kproc, tclock, kthread, eventno ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push0((kproc), tclock, eventno ) : 0UL )
#define KAAPI_EVENT_PUSH1_AT(kproc, tclock, kthread, eventno, p1 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push1((kproc), tclock, eventno, (uint64_t)(p1)) : 0UL )
#define KAAPI_EVENT_PUSH2_AT(kproc, tclock, kthread, eventno, p1, p2 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push2((kproc), tclock, eventno, (uint64_t)(p1), (uint64_t)(p2)) : 0UL)
#define KAAPI_EVENT_PUSH3_AT(kproc, tclock, kthread, eventno, p1, p2, p3 ) \
    ( ((kproc) && ((kproc)->eventbuffer) && (kproc->event_mask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push3((kproc), tclock, eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3)) : 0)

#if defined(__cplusplus)
}
#endif

#endif
