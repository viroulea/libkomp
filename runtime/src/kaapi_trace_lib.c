/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <errno.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/resource.h>
#include <pthread.h>
#include "kmp_config.h"
#include "kmp.h"
#include "kaapi_util.h"

#if defined(__linux__)
#define __USE_MISC 1
#include <sched.h>
#if LIBOMP_USE_NUMA
#include <numa.h>
#include <numaif.h>
#endif
#endif

#include <stdint.h>
#if defined(HAVE_CLOCK_GETTIME) || defined(KMP_OS_LINUX)
# include <time.h>
#else
# include <sys/time.h>
#endif
#if defined(HAVE_CLOCK_GETTIME) || defined(KMP_OS_LINUX)
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_second(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_second_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_second(t) (1e-6*(double)t.tv_usec)
#  define get_sub_second_ns(t) (1000*(uint64_t)t.tv_usec)
#endif


#include "kmp_config.h"
#if KAAPI_USE_PAPI
#include <papi.h>
#include "hw_count.h"
#endif

#include "kaapi_trace.h"
#include "kaapi_impl.h"
#include "kaapi_hashmap.h"
#include "kaapi_util.h"
#include "kaapi_atomic.h"

#if defined(__cplusplus)
extern "C" {
#endif
#define RESTRICT

/* Extra types
*/


/* ------------------------------------------------------------------------------------------- */
/*
  Global Variable
*/
kaapi_tracelib_param_t kaapi_tracelib_param;
static int fmt_listcapacity = 0;


/* Predefined group of event */
typedef struct  {
  //int countevent;
  const char*        name;
  int                code;
  kaapi_perf_idset_t mask;
  kaapi_perf_idset_t mask_pertask;
} kaapi_perfctr_group_t;

static kaapi_perfctr_group_t kaapi_perfctr_group[] = {
  { /* KAAPI_PERF_GROUP_TASK */
    .name = "TASK",
    .code = KAAPI_PERF_GROUP_TASK,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)|
//NOT computed            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PTIME)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TIDLE)|
//NOT in libOMP            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSPAWN)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKEXEC)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_SYNCINST),
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_SCHED */
    .name = "SCHED",
    .code = KAAPI_PERF_GROUP_SCHED,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CONFLICTPOP)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQOK)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQ)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALOP)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALIN) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSTEAL),
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_OMP */
    .name = "OMP",
    .code = KAAPI_PERF_GROUP_OMP,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)|
//NOT computed            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PTIME)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TIDLE)|
//NOT in libOMP            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSPAWN)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKEXEC)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_SYNCINST)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PARALLEL)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCK)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_BARRIER)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKYIELD),
    .mask_pertask = 0
  },
#if KAAPI_USE_NUMA
  { /* KAAPI_PERF_GROUP_NUMA for both thread and task */
    .name = "NUMA",
    .code = KAAPI_PERF_GROUP_NUMA,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE),
    .mask_pertask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE)

  },
#else
  {
    .name = 0,
    .code = -1,
    .mask = 0,
    .mask_pertask = 0
  },
#endif
  { /* KAAPI_PERF_GROUP_DFGBUILD */
    .name = "DFG",
    .code = KAAPI_PERF_GROUP_DFGBUILD,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_DFGBUILD)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_RDLISTINIT)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_RDLISTEXEC),
    .mask_pertask = 0
  }
#if 0 // old group offload inherited from XKaapi.
      // keep it in case of offload will becomes later
  ,
  { /* KAAPI_PERF_GROUP_OFFLOAD */
    .name = "OFFLOAD",
    .code = KAAPI_PERF_GROUP_OFFLOAD,
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_H2D)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_D2H)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_D2D)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CACHE_HIT)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CACHE_MISS),
    .mask_pertask = 0
  }
#endif
};

/* log2 of the size of hashmaps
*/
#define KAAPI_SIZE_DFGCTXT 9

/* Lock for team map and list of team
*/
static kaapi_lock_t             team_lock = KAAPI_LOCK_INITIALIZER;

/* Intialize team performance counter
*/
static kaapi_teamlinked_t* team_perfctr_head = 0;
static kaapi_teamlinked_t* team_perfctr_tail = 0;

/* hash map key/team -> kaapi_tracelib_team_t */
static kaapi_hashmap_t          tlteam_map_routine;
static kaapi_hashentries_t*     tlteam_mapentries[1<<KAAPI_SIZE_DFGCTXT];
static kaapi_hashentries_bloc_t tlteam_mapbloc;

/* hash map key/team -> kaapi_teamlinked_t */
static kaapi_hashmap_t          team_map_routine;
static kaapi_hashentries_t*     team_mapentries[1<<KAAPI_SIZE_DFGCTXT];
static kaapi_hashentries_bloc_t team_mapbloc;


/* A global perf counter = accumulation of all thread' performance counter
*/
kaapi_thread_perfctr_t*	global_perf_regs = 0;


/* Hash map for task format descriptor
*/
static kaapi_hashmap_t                fdescr_map_routine;
static kaapi_hashentries_t*           fdescr_mapentries[1<<KAAPI_SIZE_DFGCTXT];
static kaapi_hashentries_bloc_t       fdescr_mapbloc;
static kaapi_lock_t                   fdescr_map_lock = KAAPI_LOCK_INITIALIZER;

/* internal */
static unsigned int user_event_count = 0;
static unsigned int papi_event_count = 0;
static unsigned int papi_uncore_event_count = 0;
static kaapi_perf_idset_t papi_event_mask = 0;
static kaapi_perf_idset_t papi_uncore_event_mask = 0;


/* ------------------------------------------------------------------------------------------- */
/* Fwd declarations
*/
/*
*/
static int kaapi_get_events(
  const char* env,
  int task_set
);
static void kaapi_tracelib_thread_dumpcounter(
    kaapi_tracelib_thread_t*     kproc
);
static void kaapi_mt_perf_accum_perf_stat(
  kaapi_named_perfctr_t*    perf,
  const kaapi_perf_idset_t* idset
);
static void kaapi_mt_perf_accum_threadcounters(
  kaapi_perf_idset_t set,
  kaapi_perfstat_t* stat,
  const kaapi_perf_counter_t* regs
);
static void kaapi_mt_perf_cpy_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
);
/* result = value + value0 */
static void kaapi_mt_perf_add_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value,
  const kaapi_perf_counter_t* RESTRICT value0
);
/* result = value - value0 */
static void kaapi_mt_perf_sub_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value,
  const kaapi_perf_counter_t* RESTRICT value0
);
/* result += value */
static void kaapi_mt_perf_addin_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
);
/* result -= value */
static void kaapi_mt_perf_subin_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
);
/* report stats perf task types */
static void kaapi_perflib_update_perf_stat(
    uint64_t                  kid,
    int                       implicit,
    kaapi_named_perfctr_t*    perf,
    const kaapi_perf_idset_t  idset,
    kaapi_perf_counter_t*     counters
);



/* output stats 
*/
static void _kaapi_print_total(
  char* buffer, const char* title, int count, uint64_t gn[], double gsum[], double gsum2[]
);
static void _kaapi_print_stat_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t set,
  kaapi_named_perfctr_t* perf,
  uint64_t gn[], double gsum[], double gsum2[]
);
static void _kaapi_print_header(
  char* buffer,
  kaapi_perf_idset_t set
);
static void _kaapi_compute_child(void);
static void _kaapi_print_tree(
    FILE *file,
    char* buffer,
    kaapi_descrformat_t* fdescr,
    int level,
    uint64_t gn[], double gsum[], double gsum2[]
);
static void _kaapi_print_team_header(
  char* buffer,
  kaapi_perf_idset_t set
);
static void _kaapi_print_team_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t set,
  kaapi_teamlinked_t* perf
);
static void _kaapi_print_threadctr_set(
  kaapi_perf_idset_t set,
  kaapi_tracelib_thread_t* perf
);
static int _kaapi_compute_team_tree(
    int nthreads,
    kaapi_tracelib_thread_t** threads
);
static void _kaapi_print_team_tree(
    FILE *file,
    char* buffer,
    int level,
    kaapi_teamlinked_t* tperfctr
);


/* ------------------------------------------------------------------------------------------- */
/** Returns time in second
*/
double kaapi_get_elapsedtime(void)
{
  struct_time st;
  int err = gettime(&st);
  if (err !=0) return 0;
  return (double)st.tv_sec + get_sub_second(st);
}

/** Returns time in nanosecond
*/
uint64_t kaapi_get_elapsedns(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000000ULL;
  retval += get_sub_second_ns(st);
  return retval;
}

/** kaapi_get_elapsedns_since_start
    The function kaapi_get_elapsedns_since_start() returns the elapsed time in second
    since an epoch after the kaapi initialization.
*/
static uint64_t kaapi_startup_time = 0;
uint64_t kaapi_get_elapsedns_since_start(void)
{ 
  return (kaapi_get_elapsedns() - kaapi_startup_time);
}

/*
*/
static void kaapi_timelib_init(void)
{
  static int once= 0;
  if (once) return;
  once = 1;
  kaapi_startup_time = kaapi_get_elapsedns();
}

/*
*/
static void kaapi_timelib_fini(void)
{
}

static const char* _get_group( int event)
{
  if (KAAPI_EVT_MASK(event) & KAAPI_EVT_MASK_COMPUTE)
    return "COMPUTE";
  if (KAAPI_EVT_MASK(event) & KAAPI_EVT_MASK_OMP)
    return "OMP";
  if (KAAPI_EVT_MASK(event) & KAAPI_EVT_MASK_SCHED)
    return "SCHED";
  if (KAAPI_EVT_MASK(event) & KAAPI_EVT_MASK_PERFCOUNTER)
    return "PERFCTR";
  if (KAAPI_EVT_MASK(event) & KAAPI_EVT_MASK_ENERGY)
    return "ENERGY";
  return 0;
}

#if LIBOMP_USE_NUMA
/* Fwd Decl
*/
static void* _kaapi_uncore_collector(void*);
static kaapi_atomic_t kaapi_uncore_thread_finish;
static kaapi_atomic_t kaapi_uncore_thread_counter;
static pthread_t* thread_uncore = 0;
static int thread_uncore_count = 0;
#endif

/* ------------------------------------------------------------------------------------------- */
/*
*/
static void _kaapi_print_help(void)
{
  fprintf(stdout,
   "*** Welcome to the quick start of Kaapi options!\n"
   "  For more information, please visit http://kaapi.gforge.inria.fr\n"
   "  All options are controlled by environement variables. The list is the following:\n"
   "  * KAAPI_HELP | KAAPI_HELPME       : show this help\n"
   "  * KAAPI_PERF_EVENTS <perflist>    : defines performance counters to be captured by threads\n"
   "  * KAAPI_TASKPERF_EVENTS <perflist>: defines performance counters to be captured by tasks\n"
   "  * KAAPI_UNCOREPERF_EVENTS <perflist>: defines uncore performance counters to be capture by socket\n"
   "  * KAAPI_UNCOREPERF_PERIOD <int>   : defines period to capture KAAPI_UNCOREPERF_EVENTS\n"
   "  * KAAPI_RECORD_TRACE [0|1]        : to record events at runtime for postmortem analysis\n"
   "  * KAAPI_RECORD_MASK <eventlist>   : selection of events to record\n"
   "  * KAAPI_DISPLAY_PERF <kind>       : control the way performance counters are displayed\n"
   "Where:\n"
   "<perflist>: is a list (separator ',') of perf counter' names or names of perf counters' groups.\n"
   "    Kaapi following names are available:\n "
  );
  for (int i=0; i<KAAPI_PERF_ID_ENDSOFTWARE; ++i)
  {
    if (kaapi_perfctr_info[i].cmdlinename ==0) continue;
    fprintf(stdout, "\t%16.16s: code=%i, %s  %s\n",
        kaapi_perfctr_info[i].cmdlinename,
        kaapi_perfctr_info[i].eventcode,
        kaapi_perfctr_info[i].name,
        kaapi_perfctr_info[i].helpstring
    );
  }
  fprintf(stdout,
   "    Performance counter groups are:\n"
  );
  for (int i=0; i<KAAPI_PERF_GROUP_OFFLOAD; ++i)
  {
    if (kaapi_perfctr_group[i].name == 0) continue;
    fprintf(stdout, "\t%16.16s: ", kaapi_perfctr_group[i].name);
    kaapi_perf_idset_t set = kaapi_perfctr_group[i].mask;
    while (set !=0)
    {
      unsigned int idx;
      idx = __builtin_ffsl( set )-1;
      set &= ~(1UL << idx);
      if (kaapi_perfctr_info[idx].cmdlinename ==0) continue;
      fprintf(stdout, "%s%c ", kaapi_perfctr_info[idx].cmdlinename, (set ==0 ? ' ': ','));
    }
    fprintf(stdout,"\n");
  }
#if KAAPI_USE_PAPI
  fprintf(stdout,
   "    Performance counter name could be PAPI name. Please see papi_avail and related commands.\n"
  );
#else
  fprintf(stdout,
   "    The current library implementation is not configured to use PAPI, thus access to hardware\n"
   "    performance counters is not available. If you want to access those counters, please install\n"
   "    the library once configured with PAPI.\n"
  );
#endif

  fprintf(stdout,
   "<eventlist>: is a list (separator ',') of event name of groups of events.\n"
   "    Predefined groups are COMPUTE|PERFCTR|OMP|SCHED\n"
  );
  fprintf(stdout,"\t%16.16s: %s", "COMPUTE",
     "related to all events concerning task executions.\n"
     "\t                  This group is enough to build data flow graph from the trace.\n"
  );
  fprintf(stdout,"\t%16.16s: %s", "PERFCTR",
      "to be used to include performance counters in the trace.\n"
  );
  fprintf(stdout,"\t%16.16s: %s", "OMP",
   "related to all events concerning omp constructions.\n"
  );
  fprintf(stdout,"\t%16.16s: %s", "SCHED",
   "to include events concerning the dynamic scheduling.\n"
  );
  fprintf(stdout,
   "    Individual events can be added to the event list using their code.\n"
   "    The list of events is:\n"
  );
  for (int i=0; i<KAAPI_EVT_LAST; ++i)
  {
    if (kaapi_event_name[i] == 0) continue;
    const char* grp = _get_group( i );
    if (grp)
      fprintf(stdout, "\t%16i: name '%s' in %s\n", i, kaapi_event_name[i], grp );
    else
      fprintf(stdout, "\t%16i: name '%s'\n", i, kaapi_event_name[i] );
  }
  fprintf(stdout,
   "<kind>: is either\n"
   "        * no|0  : to not display information\n"
   "        * final : to display information at the end of the program execution\n"
   "\n"
  );
}


/* ------------------------------------------------------------------------------------------- */
/**
*/
int kaapi_tracelib_init(
  int gid
)
{
  static int once = 0;
  if (once) return 0;
  once = 1;

  int i, error;

  /* Update counters: undefined code */
  for (i=0; i<KAAPI_PERF_ID_ENDSOFTWARE; ++i)
    kaapi_perfctr_info[i].eventcode = i;

  if(getenv("KAAPI_HELP") || getenv("KAAPI_HELPME"))
    _kaapi_print_help();

  kaapi_tracelib_param.cpucount = 0;
  kaapi_tracelib_param.gpucount = 0;
  kaapi_tracelib_param.numaplacecount = 0;
  kaapi_tracelib_param.fmt_list = 0;
  kaapi_tracelib_param.fmt_listsize = 0;
  kaapi_tracelib_param.display_perfcounter = KAAPI_NO_DISPLAY_PERF;

  kaapi_tracelib_param.eventmask           = 0;
  kaapi_tracelib_param.recordfilename      = 0;
  kaapi_tracelib_param.papi_event_count    = 0;
  KAAPI_ATOMIC_WRITE(&kaapi_tracelib_param.nthreads, 0);
  kaapi_tracelib_param.threads = (kaapi_tracelib_thread_t**)calloc( 512, sizeof(kaapi_tracelib_thread_t*) );

  /* allows to have the delay between init/fini */
  kaapi_timelib_init();

  /* map for format descriptor */
  kaapi_hashmap_init( &fdescr_map_routine,
                      fdescr_mapentries,
                      KAAPI_SIZE_DFGCTXT,
                      &fdescr_mapbloc );

  /* map for team  */
  kaapi_hashmap_init( &tlteam_map_routine,
                      tlteam_mapentries,
                      KAAPI_SIZE_DFGCTXT,
                      &tlteam_mapbloc );

  /* map for team  */
  kaapi_hashmap_init( &team_map_routine,
                      team_mapentries,
                      KAAPI_SIZE_DFGCTXT,
                      &team_mapbloc );


  kaapi_tracelib_param.gid = gid;
  kaapi_tracelib_param.cpucount = -1;
  kaapi_tracelib_param.gpucount = -1;
#if LIBOMP_USE_NUMA
  kaapi_tracelib_param.numaplacecount = numa_num_configured_nodes();
#else
  kaapi_tracelib_param.numaplacecount = 1;
#endif

  /* generating prefix for events' files 
  */
  kaapi_tracelib_param.recordfilename = getenv("KAAPI_RECORD_PREFIX");
  if (kaapi_tracelib_param.recordfilename ==0)
  {
    char filename[128];
    const char* uname = getenv("USER");
    if (uname !=0)
      sprintf(filename,"/tmp/events.%s.%i", uname, kaapi_tracelib_param.gid );
    else
      sprintf(filename,"/tmp/events.%i", kaapi_tracelib_param.gid );
    kaapi_tracelib_param.recordfilename = strdup(filename);
  }

  /* perf counters initialization */
  kaapi_perf_idset_zero( &kaapi_tracelib_param.perfctr_idset ) ;
  kaapi_perf_idset_zero( &kaapi_tracelib_param.taskperfctr_idset ) ;
  kaapi_perf_idset_zero( &kaapi_tracelib_param.uncoreperfctr_idset ) ;

#if KAAPI_USE_PAPI
  error = PAPI_library_init(PAPI_VER_CURRENT);
  kaapi_assert(error == PAPI_VER_CURRENT);
  
  error = PAPI_thread_init(pthread_self);
  kaapi_assert(error == PAPI_OK);

  kaapi_assert(sizeof(long_long) == sizeof(kaapi_perf_counter_t) );
#endif

  error = kaapi_get_events("KAAPI_PERF_EVENTS", 0);
  kaapi_assert(0 == error);

  /* always add task counter into the global set of counter */
  /* may be user add counter perf task (e.g. etf WSPUSH strategy) */
  error = kaapi_get_events("KAAPI_TASKPERF_EVENTS", 1);
  kaapi_assert(0 == error);

  error = kaapi_get_events("KAAPI_UNCOREPERF_EVENTS", 2);
  kaapi_assert(0 == error);

  char* str = getenv("KAAPI_UNCOREPERF_PERIOD");
  if (str)
  {
    error = kaapi_parse_delay(&str, &kaapi_tracelib_param.uncore_period);
    kaapi_assert(0 != error);
  }

  global_perf_regs = (kaapi_thread_perfctr_t*)malloc( sizeof(kaapi_thread_perfctr_t));
  memset( global_perf_regs, 0, sizeof(kaapi_thread_perfctr_t) );

  /* event mask */
  kaapi_tracelib_param.eventmask = 0;
  if ((getenv("KAAPI_RECORD_TRACE") !=0) && !strcasecmp(getenv("KAAPI_RECORD_TRACE"),"1"))
  {
    if (getenv("KAAPI_RECORD_MASK") !=0)
    {
      /* actual grammar:
         descr[,descr]*
         descr -> groupname | eventno
         eventno is an integer less than 2^sizeof(kaapi_event_mask_type_t)
         grammar must be more complex using predefined set
      */
      uint64_t mask = 0;
      char* name = getenv("KAAPI_RECORD_MASK");
      bool err = kaapi_parse_listkeywords( &mask, &name, ',',
         4,
           "COMPUTE", (uint64_t)KAAPI_EVT_MASK_COMPUTE,
           "SCHED",   (uint64_t)KAAPI_EVT_MASK_SCHED,
           "OMP",     (uint64_t)KAAPI_EVT_MASK_OMP,
           "PERFCTR", (uint64_t)KAAPI_EVT_MASK_PERFCOUNTER
//           "OFFLOAD", (uint64_t)KAAPI_EVT_MASK_OFFLOAD
      );
      if (err ==false)
      {
        fprintf(stderr, "*** Kaapi: mal formed mask list 'KAAPI_RECORD_MASK': '%s'\n",
          getenv("KAAPI_RECORD_MASK")
        );
        return EINVAL;
      }
      /* always add startup set */
      kaapi_tracelib_param.eventmask = mask|KAAPI_EVT_MASK_STARTUP;
    }
  }

  kaapi_tracelib_param.fmt_list = 0;
  kaapi_tracelib_param.fmt_listsize = 0;
  fmt_listcapacity = 0;

  kaapi_tracelib_param.task_event_count
    = __builtin_popcountl(kaapi_tracelib_param.taskperfctr_idset);

  /* init recorder module */
  kaapi_eventrecorder_init();

  char* displayperf;
  int displayperf_value;
  displayperf = getenv("KAAPI_DISPLAY_PERF");
  if (displayperf)
  /* A revoir le parsing des noms & du meaning. Utiliser kaapi_parse_listkeywords
     pour choisir à la fois la destination (stdout,file) et le mode de sortie
  */
  if (!kaapi_parse_perfcounter(&displayperf, &displayperf_value))
  {
    fprintf(stderr, "***Kaapi: bad value for variable KAAPI_DISPLAY_PERF. Use 'false'\n");
    kaapi_tracelib_param.display_perfcounter = KAAPI_NO_DISPLAY_PERF;
  }
  kaapi_tracelib_param.display_perfcounter = (kaapi_display_perf_value_t)displayperf_value;

#if LIBOMP_USE_NUMA
  /* Start Monitoring thread uncore event that should be collected once... */
  if (kaapi_tracelib_param.uncoreperfctr_idset || papi_uncore_event_mask)
  {
    int nnodes = numa_num_configured_nodes();
    thread_uncore = (pthread_t*)malloc(sizeof(pthread_t)*nnodes);
    KAAPI_ATOMIC_WRITE(&kaapi_uncore_thread_finish, 0);
    KAAPI_ATOMIC_WRITE(&kaapi_uncore_thread_counter, 0);
    thread_uncore_count = 0;
    for (int i=0; i<nnodes; ++i)
    {
      pthread_attr_t attr;
      pthread_attr_init(&attr);
      KAAPI_ATOMIC_INCR(&kaapi_uncore_thread_counter);
      kaapi_assert(pthread_create(&thread_uncore[i], &attr, _kaapi_uncore_collector, (void*)(intptr_t)i) ==0);
      ++thread_uncore_count;
    }
    while (KAAPI_ATOMIC_READ(&kaapi_uncore_thread_counter) !=0)
      sched_yield();
  }
#endif

  return 0;
}


/* ------------------------------------------------------------------------------------------- */
/** Finish trace. Assume that threads have reach the barrier and flush
    their event buffers.
*/
void kaapi_tracelib_fini(void)
{
  static int once = 0;
  if (once) return;
  once = 1;

  FILE *file = 0;
  char buffer[8192];
  char filename[128];

#if LIBOMP_USE_NUMA
  KAAPI_ATOMIC_WRITE(&kaapi_uncore_thread_finish, 1);
  for (int i=0; i<thread_uncore_count; ++i)
  {
    void* res;
    kaapi_assert(pthread_join(thread_uncore[i], &res) ==0);
  }
#endif

#if KAAPI_USE_PAPI
  PAPI_shutdown();
#endif

  /* Display stat per task */
  if (kaapi_tracelib_param.display_perfcounter != KAAPI_NO_DISPLAY_PERF)
  {
    sprintf(filename, "stat.%i", getpid());
    file = fopen(filename,"w");
    if (file == 0)
      fprintf(stderr,"*** Kaapi: cannot open stat file '%s'\n", filename);
    fprintf(file,"# Statistics about process %i. Duration: %e(s)\n",
        getpid(),
        1e-9*((double)kaapi_get_elapsedns_since_start())
    );
  }

  if (file !=0)
  {
    /* accumulate over all teams */
    int nteam = _kaapi_compute_team_tree(
        KAAPI_ATOMIC_READ(&kaapi_tracelib_param.nthreads),
        kaapi_tracelib_param.threads
    );
    if (nteam)
    {
      fprintf(file, "#---------- Teams' Performance Counter\n");

      _kaapi_print_team_header( buffer, kaapi_tracelib_param.perfctr_idset );
      fprintf(file, "%s\n",buffer);
      kaapi_teamlinked_t* curr = team_perfctr_head;
      while (curr !=0)
      {
        if (curr->parent ==0) /* root */
          _kaapi_print_team_tree(file, buffer, 0, curr );
        curr = curr->sibling;
      }
    }

    /* Display per task counters informations */
    if (kaapi_tracelib_param.taskperfctr_idset !=0)
    {
      int perfctr_cnt = kaapi_tracelib_param.task_event_count;
      int banner0;
      int banner1;
      size_t i;

      /* display format of the output */
      fprintf(file, "\n");

      uint64_t total_gn[perfctr_cnt];
      double total_gsum[perfctr_cnt];
      double total_gsum2[perfctr_cnt];
      memset( total_gn, 0, sizeof(total_gn));
      memset( total_gsum, 0, sizeof(total_gsum));
      memset( total_gsum2, 0, sizeof(total_gsum2));

      banner0 = 0;
      uint64_t gn[perfctr_cnt];
      double gsum[perfctr_cnt];
      double gsum2[perfctr_cnt];
      memset( gn, 0, sizeof(gn));
      memset( gsum, 0, sizeof(gsum));
      memset( gsum2, 0, sizeof(gsum2));

      for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
      {
        if (kaapi_tracelib_param.fmt_list[i]->implicit)
          continue;
        kaapi_named_perfctr_t* perf = kaapi_tracelib_param.fmt_list[i]->perfctr;
        if (!banner0)
        {
          fprintf(file,"# --------------- Explicit task\n");
          _kaapi_print_header( buffer, kaapi_tracelib_param.taskperfctr_idset );
          fprintf(file, "%s\n", buffer);
          banner0 = 1;
        }

        kaapi_mt_perf_accum_perf_stat( perf, &kaapi_tracelib_param.taskperfctr_idset);

        if (perf->stats.count !=0)
        {
          _kaapi_print_stat_set( buffer, 0, kaapi_tracelib_param.taskperfctr_idset, perf, gn, gsum, gsum2 );
          fprintf(file, "%s\n",buffer);
        }
      }
      for (i = 0; i<perfctr_cnt; ++i)
      {
        total_gn[i] += gn[i];
        total_gsum[i] += gsum[i];
        total_gsum2[i] += gsum2[i];
      }
      if (banner0 ==1)
      {
        _kaapi_print_total( buffer, "   Sub total", perfctr_cnt, gn, gsum, gsum2);
        fprintf(file, "%s\n\n",buffer);
      }

      banner1 = 0;
      memset( gn, 0, sizeof(gn));
      memset( gsum, 0, sizeof(gsum));
      memset( gsum2, 0, sizeof(gsum2));

      /* */
      _kaapi_compute_child();
      for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
      {
        if (kaapi_tracelib_param.fmt_list[i]->implicit ==0)
          continue;
        kaapi_descrformat_t* fdescr = kaapi_tracelib_param.fmt_list[i];
        if (fdescr->parent) /* print by child/sibling info */
          continue;
        if (!banner1)
        {
          fprintf(file,"# --------------- Implicit task\n");
          _kaapi_print_header( buffer, kaapi_tracelib_param.taskperfctr_idset );
          fprintf(file, "%s\n", buffer);
          banner1 = 1;
        }
        _kaapi_print_tree( file, buffer, fdescr, 0, gn, gsum, gsum2 );
      }

      for (i = 0; i<perfctr_cnt; ++i)
      {
        total_gn[i] += gn[i];
        total_gsum[i] += gsum[i];
        total_gsum2[i] += gsum2[i];
      }
      if (banner1 == 1)
      {
        _kaapi_print_total( buffer, "   Sub total", perfctr_cnt, gn, gsum, gsum2);
        fprintf(file, "%s\n\n",buffer);
      }

      if ((banner0 == 1)||(banner1 == 1))
      {
        _kaapi_print_total( buffer, "   Total", perfctr_cnt, total_gn, total_gsum, total_gsum2);
        fprintf(file, "\n%s\n\n\n",buffer);
      }
    }

//    fprintf(file, "###--- Global performance counters\n");
//    kaapi_display_rawperf(file, (const kaapi_thread_perfctr_t*)global_perf_regs);

    printf("File: '%s' generated\n", filename);
  }

  if (kaapi_tracelib_param.eventmask)
    kaapi_eventrecorder_fini();

  kaapi_timelib_fini();
}

/* ------------------------------------------------------------------------------------------- */
/**
*/
kaapi_tracelib_thread_t* kaapi_tracelib_thread_init(
    uint64_t               kid,
    int                    cpu,     /* if protype ==2, used to attach event to cpu */
    int                    numaid,     /* if protype ==2, used to attach event to cpu */
    int                    proctype /* 0: cpu, 1: gpu, 2: uncore collector */
)
{
  kaapi_tracelib_thread_t* ctxt;

//libOMP move thread at the start of the parallel region
//printf("%i/%i:: thread on numa:%i <-> %i\n", kid, sched_getcpu(), numaid, __kmp_cpu2node(sched_getcpu()));

  /* reuse team and per thread data. Init or re-init papi in the both case.
  */
  size_t size = sizeof(kaapi_tracelib_thread_t);
  int err = posix_memalign( (void**)&ctxt, getpagesize(), size );
  if (err !=0) return 0;

  memset( ctxt, 0, size );
  ctxt->kid          = kid;
  ctxt->numaid       = numaid;
  ctxt->cpu          = cpu;
  ctxt->ptype        = proctype;
  ctxt->mode         = -1;
  ctxt->papi_event_count  = 0;

  switch (proctype) {
    case 2: /* uncore collector */
      ctxt->perfset         = kaapi_tracelib_param.uncoreperfctr_idset;
      ctxt->task_perfset    = 0;
      ctxt->event_mask      = kaapi_tracelib_param.eventmask;
      ctxt->papi_event_mask = papi_uncore_event_mask;
      ctxt->papi_event_count= papi_uncore_event_count;
    break;

    case 1: /* GPU */
      ctxt->perfset         = 0;
      ctxt->task_perfset    = 0;
      ctxt->event_mask      = 0;
      ctxt->papi_event_mask = 0;
      ctxt->papi_event_count= 0;
    break;

    case 0:
    default:
      ctxt->perfset         = kaapi_tracelib_param.perfctr_idset;
      ctxt->task_perfset    = kaapi_tracelib_param.taskperfctr_idset;
      ctxt->event_mask      = kaapi_tracelib_param.eventmask;
      ctxt->papi_event_mask = papi_event_mask;
      ctxt->papi_event_count= papi_event_count;
    break;
  }
  ctxt->head         = 0;

  if (kaapi_tracelib_param.eventmask)
    ctxt->eventbuffer = kaapi_event_openbuffer((int)kid, proctype );

#if KAAPI_USE_PAPI
  int papi_event_codes[KAAPI_MAX_HWCOUNTERS];
  if (ctxt->papi_event_mask)  /* papi_event_count */
  {
    int err;
    PAPI_option_t opt;

    /* register the thread */
    err = PAPI_register_thread();
    kaapi_assert(PAPI_OK == err);

    /* create event set */
    ctxt->papi_event_set = PAPI_NULL;
    err = PAPI_create_eventset(&ctxt->papi_event_set);
    kaapi_assert(PAPI_OK == err);

    if (proctype ==0)
    {
      /* set cpu as the default component. mandatory in newer interfaces. */
      err = PAPI_assign_eventset_component(ctxt->papi_event_set, 0);
      kaapi_assert(PAPI_OK == err);

      /* thread granularity */
      memset(&opt, 0, sizeof(opt));
      opt.granularity.def_cidx = ctxt->papi_event_set;
      opt.granularity.eventset = ctxt->papi_event_set;
      opt.granularity.granularity = PAPI_GRN_THR;
      err = PAPI_set_opt(PAPI_GRANUL, &opt);
      kaapi_assert(PAPI_OK == err);

      /* user domain */
      memset(&opt, 0, sizeof(opt));
      opt.domain.eventset = ctxt->papi_event_set;
      opt.domain.domain = PAPI_DOM_USER;
      err = PAPI_set_opt(PAPI_DOMAIN, &opt);
      kaapi_assert(PAPI_OK == err);
    }
    else if (proctype ==2)
    {
      /* Find the uncore PMU */
      int uncore_cidx=PAPI_get_component_index("perf_event_uncore");
      if (uncore_cidx<0) {
        fprintf(stderr, "*** PAPI: perf_event_uncore component not found\n");
        return 0;
      }
      /* Check if component disabled */
      const PAPI_component_info_t *info =PAPI_get_component_info(uncore_cidx);
      if (info->disabled) {
        fprintf(stderr,"*** PAPI: perf_event_uncore component is disabled\n");
      }

      /* set cpu as the default component. mandatory in newer interfaces. */
      err = PAPI_assign_eventset_component(ctxt->papi_event_set, uncore_cidx);
      kaapi_assert(PAPI_OK == err);

      PAPI_cpu_option_t cpu_opt;
      memset(&cpu_opt, 0, sizeof(cpu_opt));
      cpu_opt.eventset=ctxt->papi_event_set;
      cpu_opt.cpu_num=cpu;
      err = PAPI_set_opt(PAPI_CPU_ATTACH,(PAPI_option_t*)&cpu_opt);
      kaapi_assert(PAPI_OK == err);

#if 0
      memset(&opt, 0, sizeof(opt));
      opt.granularity.def_cidx = 0;
      opt.granularity.eventset = ctxt->papi_event_set;
      opt.granularity.granularity = PAPI_GRN_SYS;
      err = PAPI_set_opt(PAPI_GRANUL, &opt);

      kaapi_assert(PAPI_OK == err);
      memset(&opt, 0, sizeof(opt));
      opt.domain.def_cidx = 0;
      opt.domain.eventset = ctxt->papi_event_set;
      opt.domain.domain = PAPI_DOM_ALL;
      err = PAPI_set_opt(PAPI_DOMAIN, &opt);
      kaapi_assert(PAPI_OK == err);
#endif
    }

    /* configure the papi event set */
    unsigned int i;
    int count =0;
    for ( i=KAAPI_PERF_ID_PAPI_BASE; i<KAAPI_PERF_ID_MAX; ++i)
    {
      if (kaapi_perf_idset_test( &ctxt->perfset, i))
      {
printf("Attach event: '%s'\n", kaapi_perfctr_info[i].name);
        papi_event_codes[count++] = kaapi_perfctr_info[i].eventcode;
      }      
    }
    kaapi_assert_debug( count == papi_event_count );

    err = PAPI_add_events(ctxt->papi_event_set, papi_event_codes, count);
    if (err != PAPI_OK) 
      fprintf(stderr,"PAPI error code:%i, could not add events in set. Msg: %s\n",err, PAPI_strerror(err));
    kaapi_assert(PAPI_OK == err);
  }
#endif

  int idx = KAAPI_ATOMIC_INCR_ORIG(&kaapi_tracelib_param.nthreads);
  kaapi_assert( idx < 512 ); /* see tracelib_init */
  kaapi_tracelib_param.threads[idx] = ctxt;
  return ctxt;
}

/*
*/
void kaapi_tracelib_thread_start( kaapi_tracelib_thread_t* kproc )
{
  /* reset all counters in both sys/usr states */
  memset( &kproc->perf_regs, 0, sizeof(kproc->perf_regs) );
  kproc->mode = KAAPI_PERF_ID_TIDLE;
  kproc->tstart = kaapi_get_elapsedns();

#if KAAPI_USE_PAPI
  if (kproc->papi_event_count)
  {
    int err = PAPI_start(kproc->papi_event_set);
    if (err != PAPI_OK) 
      fprintf(stderr,"PAPI error code:%i, could not start counting. Msg: %s\n",err, PAPI_strerror(err));
    
    kaapi_assert(PAPI_OK == err);
  }
#endif
#if LIBOMP_USE_NUMA
  KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_KPROC_START, kproc->ptype, __kmp_cpu2node(sched_getcpu()) );
#else
  KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_KPROC_START, kproc->ptype, 0 );
#endif
//printf("START %i %i task on numa:%i <-> %i\n", kproc->kid, sched_getcpu(), kproc->numaid, numa_node_of_cpu(kproc->kid));
}

/*
*/
static void kaapi_tracelib_thread_synccounter(
    kaapi_tracelib_thread_t*     kproc,
    int                          stopflag
)
{
  kaapi_assert_debug( sizeof(kproc->perfset) == 64/8 ); /* __builtin functions on 64 bits */

  kaapi_perf_counter_t* regs = &kproc->perf_regs[0];

  /* stop timer TIDLE or TWORK */
  uint64_t t = kaapi_get_elapsedns();
  regs[kproc->mode] += t - kproc->tstart;
  kproc->tstart = t;

#if KAAPI_USE_PAPI
  if (kproc->papi_event_count)
  {
    /* accumulate papi counter to kproc counters */
    int err;
    if (stopflag)
      err = PAPI_stop(kproc->papi_event_set, (long_long*)(regs + KAAPI_PERF_ID_PAPI_BASE));
    else
      err = PAPI_accum(kproc->papi_event_set, (long_long*)(regs + KAAPI_PERF_ID_PAPI_BASE));
    kaapi_assert(PAPI_OK == err);
  }
#endif
}


/*
*/
void kaapi_tracelib_thread_stop(
    kaapi_tracelib_thread_t*     kproc
)
{
  kaapi_tracelib_thread_synccounter(kproc, 1);
  kproc->mode = -1;

  if (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER))
    kaapi_tracelib_thread_dumpcounter( kproc );
  KAAPI_EVENT_PUSH0(kproc, 0, KAAPI_EVT_KPROC_STOP );
}


/*
*/
void kaapi_tracelib_thread_read(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_perf_idset_t           idset,
    kaapi_perf_counter_t*        regs
)
{
  kaapi_assert_debug( sizeof(idset) == 64/8 ); /* __builtin functions on 64 bits */

  /* ready first software counters */
  unsigned int i;
  kaapi_perf_idset_t set = idset & ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1);

  /* recopy software registers */
  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if (i == kproc->mode)
    {
      *regs = kproc->perf_regs[i] + kaapi_get_elapsedns()-kproc->tstart;
    }
    else
      *regs = kproc->perf_regs[i];
    ++regs;
  }

#if KAAPI_USE_PAPI
  if (idset & kproc->papi_event_mask)
  {
    /* not that event counts between kaapi_tracelib_thread_init and here represent the
       cost to this thread wait all intialization of other threads, set setconcurrency.
       After this call we assume that we are counting.
    */
    kaapi_assert(PAPI_OK == PAPI_read(kproc->papi_event_set, (long_long*)regs));
    regs += kproc->papi_event_count;
  }
#endif
}

/* regs = current - regs
*/
void kaapi_tracelib_thread_readsub(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_perf_idset_t           idset,
    kaapi_perf_counter_t*        regs
)
{
  kaapi_assert_debug( sizeof(idset) == 64/8 ); /* __builtin functions on 64 bits */

  /* ready first software counters */
  unsigned int i;
  kaapi_perf_idset_t set = idset & ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1);

  /* recopy software registers */
  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if (i == kproc->mode)
    {
      kaapi_perf_counter_t val = kproc->perf_regs[i] + kaapi_get_elapsedns()-kproc->tstart;
      *regs = val - *regs;
    }
    else
      *regs = kproc->perf_regs[i] - *regs;
    ++regs;
  }

#if KAAPI_USE_PAPI
  if (idset & kproc->papi_event_mask)
  {
    kaapi_perf_counter_t tmp[kproc->papi_event_count];
    /* not that event counts between kaapi_tracelib_thread_init and here represent the
       cost to this thread wait all intialization of other threads, set setconcurrency.
       After this call we assume that we are counting.
    */
    kaapi_assert(PAPI_OK == PAPI_read(kproc->papi_event_set, (long_long*)tmp));
    for (i=0; i<kproc->papi_event_count; ++i)
      regs[i] = tmp[i] - regs[i];
    regs += kproc->papi_event_count;
  }
#endif
}


/*
*/
static void kaapi_tracelib_thread_accum(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_perf_idset_t           idset,
    kaapi_perf_counter_t*        regs,
    kaapi_perf_counter_t*        read
)
{
  kaapi_assert_debug( sizeof(idset) == 64/8 ); /* __builtin functions on 64 bits */

  /* ready first software counters */
  unsigned int i;
  kaapi_perf_idset_t set = idset & ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1);

  /* recopy software registers */
  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if (i == kproc->mode)
    {
      uint64_t t = kaapi_get_elapsedns();
      *regs += kproc->perf_regs[i] + (t-kproc->tstart);
      kproc->tstart = t;
    } else
      *regs += kproc->perf_regs[i];
    ++regs;
    kproc->perf_regs[i] = 0;
  }

#if KAAPI_USE_PAPI
  if (idset & papi_event_mask)
  {
    const int err = PAPI_accum(
      kproc->papi_event_set,
      (long_long*)(regs + KAAPI_PERF_ID_PAPI_BASE)
    );
    kaapi_assert(PAPI_OK == err);
    regs += kproc->papi_event_count;
  }
#endif
}



/* Switch to count time from tidle to twork
   Return the time take into accounting
*/
/* Switch to mode state
*/
int kaapi_tracelib_thread_switchstate(
    kaapi_tracelib_thread_t*     kproc
)
{
  if (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER))
  {
    kaapi_tracelib_thread_synccounter( kproc, 0 );
    kaapi_tracelib_thread_dumpcounter( kproc );
  }
  else
  {
    /* switch timer TIDLE or TWORK */
    uint64_t t = kaapi_get_elapsedns();
    kproc->perf_regs[kproc->mode] += t - kproc->tstart;
    kproc->tstart = t;
  }
  kproc->mode = 1 - kproc->mode; /* TIDLE = 1 & TWORK = 0 */
  return 1-kproc->mode;
}



/*
*/
void kaapi_tracelib_thread_fini(
    kaapi_tracelib_thread_t*     kproc
)
{
  if (kproc->event_mask)
    kaapi_event_closebuffer(kproc->eventbuffer);

#if KAAPI_USE_PAPI
  if (kproc->papi_event_count)
  {
    PAPI_cleanup_eventset(kproc->papi_event_set);
    PAPI_destroy_eventset(&kproc->papi_event_set);
    kproc->papi_event_set = PAPI_NULL;
    kproc->papi_event_count = 0;
    PAPI_unregister_thread();
  }
#endif
}


/*
*/
void kaapi_tracelib_thread_state (
    kaapi_tracelib_thread_t*     kproc,
    uint8_t                      begend, /* 0==begin, 1==end, 2==running */
    uint32_t                     cpu,
    uint32_t                     node,
    uint64_t                     state
)
{
  kaapi_event_t* evt = KAAPI_EVENT_GET(kproc, 0, KAAPI_EVT_THREAD_STATE );
  if (evt)
  {
    evt->u.s.d0.i8[0]  = begend;
    evt->u.s.d1.i32[0] = cpu;
    evt->u.s.d1.i32[1] = node;
    evt->u.s.d2.i64[0] = state;
    KAAPI_EVENT_PUSH(kproc, 0, KAAPI_EVT_THREAD_STATE);
  }
}


/*
*/
void kaapi_tracelib_thread_stealop (
    kaapi_tracelib_thread_t*     kproc,
    uint8_t                      op,
    uint32_t                     cpu,
    uint32_t                     node,
    uint32_t                     victim_level,
    uint32_t                     victim_level_id
)
{
  kaapi_event_t* evt = KAAPI_EVENT_GET(kproc, 0, KAAPI_EVT_TASK_STEALOP );
  if (evt)
  {
    evt->u.s.d0.i8[0]  = op;
    evt->u.s.d1.i32[0] = cpu;
    evt->u.s.d1.i32[1] = node;
    evt->u.s.d2.i32[0] = victim_level;
    evt->u.s.d2.i32[1] = victim_level_id;
    KAAPI_EVENT_PUSH(kproc,0, KAAPI_EVT_TASK_STEALOP);
  }
}





/* ------------------------------------------------------------------------------------------- */
/*
*/
kaapi_tracelib_team_t* kaapi_tracelib_team_init(
    kaapi_tracelib_thread_t* kproc,
    void* key,
    kaapi_tracelib_team_t* parent_team,
    void* (*routine)(),
    const char* psource,
    const char* name,
    char* (*filter_name)(char*, int, const char*, const char*)
)
{
  kaapi_tracelib_team_t* team = (kaapi_tracelib_team_t*)malloc( sizeof(kaapi_tracelib_team_t) );
  team->key        = key;
  team->parent_team = parent_team;
  memset(&team->stats, 0, sizeof(kaapi_perfstat_t));
  if (filter_name && psource)
  {
    char buffer[128];
    team->name = strdup(filter_name(buffer, 128, psource, name));
  }
  else if (psource && !name)
    team->name  = strdup(psource);
  else if (name)
    team->name  = strdup(name);
  else
    team->name  = (char*)"<undefined>";

  team->next = kproc->head;
  kproc->head = team;
  return team;
}


/**
*/
int kaapi_tracelib_team_fini(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t* team_perfctr
)
{
  return 0;
}



/*
*/
void kaapi_tracelib_team_start(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t*   team,
    kaapi_tracelib_team_t*   parent,
    int                      parallel_id
)
{
  KAAPI_EVENT_PUSH2( kproc, 0, KAAPI_EVT_PARALLEL, 1, parallel_id );
//Here information is correct
//printf("%i/%i:: parallel on numa:%i <-> %i\n", kproc->kid, sched_getcpu(), kproc->numaid, __kmp_cpu2node(sched_getcpu()));
  kaapi_tracelib_thread_read(kproc, kproc->perfset, &team->perf0[0]);
  if (parent)
    /* parent->perf = team->perf0 - parent->perf0 */
    kaapi_mt_perf_sub_counters(kproc->perfset, &parent->perf[0], &team->perf0[0], &parent->perf0[0] );
}

/*
*/
void kaapi_tracelib_team_stop(
    kaapi_tracelib_thread_t* kproc,
    kaapi_tracelib_team_t*   team,
    kaapi_tracelib_team_t*   parent,
    int                      parallel_id
)
{
  KAAPI_EVENT_PUSH2( kproc, 0, KAAPI_EVT_PARALLEL, 0, parallel_id );
  kaapi_perf_counter_t tmp[ kaapi_tracelib_thread_idsetsize(kproc) ];
  kaapi_perf_counter_t* readbuff;
  if (parent)
    readbuff = &parent->perf0[0];
  else
    readbuff = &tmp[0];
  kaapi_tracelib_thread_read(kproc, kproc->perfset, readbuff);
  kaapi_mt_perf_sub_counters(kproc->perfset, &team->perf[0], readbuff, &team->perf0[0]);
  kaapi_mt_perf_accum_threadcounters(kproc->perfset, &team->stats, &team->perf[0]);

}

/* ------------------------------------------------------------------------------------------- */
/* 
*/

/*
*/
__thread uint64_t task_id = 0;
kaapi_task_id_t kaapi_tracelib_newtask_id(void)
{
  kaapi_task_id_t value = (kaapi_task_id_t)(++task_id | (((uint64_t)pthread_self()) << 16));
  return value;
}


/* 
*/
void kaapi_tracelib_task_begin(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    uint64_t                     fmtid,
    int8_t                       isexplicit,
    int8_t                       kind,
    int8_t                       strict,
    uint64_t                     tag,
    uint8_t                      rsrc,
    uint64_t*                    size_rsrc,
    kaapi_perf_counter_t*        perfctr0,
    kaapi_perf_counter_t*        parent_perfctr
)
{
  if (kproc->task_perfset)
  {
    if (parent_perfctr !=0)
    {
      /* parent <- parent - perfctr0 */
      kaapi_mt_perf_subin_counters( kproc->task_perfset,
                                  parent_perfctr,
                                  perfctr0
      );
    }
    kaapi_tracelib_thread_read(   kproc,
                                  kproc->task_perfset,
                                  perfctr0);
    if (parent_perfctr !=0)
    {
      /* parent <- parent + current counter */
      kaapi_mt_perf_addin_counters( kproc->task_perfset,
                                  parent_perfctr,
                                  perfctr0
      );
    }
  }
  kaapi_event_t* evt = KAAPI_EVENT_GET(kproc, 0, KAAPI_EVT_TASK_BEG );
  if (evt)
  {
    evt->u.s.d0.i = (uintptr_t)task;
    evt->u.s.d1.u = fmtid;
    evt->u.s.d2.i8[0] = isexplicit;
    evt->u.s.d2.i8[1] = kind;
    evt->u.s.d2.i8[2] = strict;
    evt->u.s.d3.u     = tag;
    KAAPI_EVENT_PUSH(kproc,0, KAAPI_EVT_TASK_BEG);
  }
//  if (rsrc & 0x1) /* dram only tracing */
//  {
//    KAAPI_EVENT_PUSH3(kproc, 0, KAAPI_EVT_TASK_ALLOC, (uintptr_t)task, size_rsrc[0], 0 );
//  }
}


/* */
void kaapi_tracelib_task_end(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    uint8_t                      rsrc,
    uint64_t*                    size_rsrc,
    kaapi_descrformat_t*         fdescr,
    kaapi_perf_counter_t*        perfctr0,
    kaapi_perf_counter_t*        task_perfctr
)
{
  kaapi_named_perfctr_t* named_perfctr = fdescr->perfctr;
  if (kproc->task_perfset)
  {
    /* accumulate in task_perfctr -= perfctr0 */
    kaapi_mt_perf_subin_counters( kproc->task_perfset,
                                task_perfctr,
                                perfctr0
    );
    kaapi_tracelib_thread_read( kproc,
                                kproc->task_perfset,
                                perfctr0);

    /* accumulate in task_perfctr += current */
    kaapi_mt_perf_addin_counters( kproc->task_perfset,
                                task_perfctr,
                                perfctr0
    );

    if (named_perfctr !=0)
      kaapi_perflib_update_perf_stat(
        kproc->kid,
        fdescr->implicit,
        named_perfctr,
        kproc->task_perfset,
        task_perfctr
      );
  }
  KAAPI_EVENT_PUSH_PERFCTR(kproc, 0, task, &kproc->task_perfset, task_perfctr );
//  if (rsrc & 0x1) /* dram only tracing */
//  {
//    KAAPI_EVENT_PUSH3(kproc, 0, KAAPI_EVT_TASK_FREE, (uintptr_t)task, size_rsrc[0], 0 );
//  }
  KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_TASK_END, task, kproc->numaid );
//printf("%i:%i:: task on numa:%i <-> %i\n", kproc->kid, sched_getcpu(), kproc->numaid, __kmp_cpu2node(sched_getcpu()));
//kproc->numaid );
  KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_TASKEXEC, 1);
}


/*
*/
void kaapi_tracelib_task_depend(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              source,
    kaapi_task_id_t              sink
)
{
    KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_TASK_SUCC, source, sink );
}


/*
*/
void kaapi_tracelib_task_attr(
    kaapi_tracelib_thread_t* kproc,
    kaapi_task_id_t task,
    int kind,
    int64_t value
)
{
    KAAPI_EVENT_PUSH3(kproc, 0, KAAPI_EVT_TASK_ATTR, task, kind, value );
}

/*
*/
#if defined(__linux__) && KAAPI_USE_NUMA
static unsigned int kaapi_numa_getpage_id(const void* addr)
{
  int mode = -1;
  const int err = get_mempolicy(&mode, (unsigned long*)0, 0, (void* )addr, MPOL_F_NODE | MPOL_F_ADDR);

  if (err)
    return (unsigned int)-1;

  /* convert to internal kaapi identifier */
  return mode;
}
#endif


static const uint64_t MASK_PERF_READWRITE_ACCESS = 
   KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ)  | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
   KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE);


static void __kaapi_dump_access(
    kaapi_tracelib_thread_t*     kproc,
    int                          local_numaid,
    kaapi_task_id_t              task,
    int                          count,
    void*                        deps,
    void                       (*decoder)(void*, int, void**, size_t*, int*)
)
{
  for (int i=0; i<count; ++i)
  {
    int mode = KAAPI_ACCESS_MODE_VOID;
    void* addr = 0;
    size_t len;
    decoder( deps, i, &addr, &len, &mode);
    if (mode & KAAPI_ACCESS_MODE_V)
      continue;

#if defined(__linux__) && KAAPI_USE_NUMA
    unsigned int numaid = kaapi_numa_getpage_id( addr );
#else
    unsigned int numaid = 0;
#endif
    KAAPI_EVENT_PUSH4(kproc, 0, KAAPI_EVT_TASK_ACCESS, task, mode, addr, numaid );

    /* how to count remote access if numa information not available ? */
    if (numaid == (unsigned int)-1) return;

    if (kaapi_perf_idset_test_mask(&kproc->perfset, MASK_PERF_READWRITE_ACCESS))
    {
      if ((numaid == local_numaid) || (numaid == (unsigned int)-1))
      {
        if (KAAPI_ACCESS_IS_READ(mode))
          KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_LOCAL_READ, 1);
        if (KAAPI_ACCESS_IS_WRITE(mode))
          KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_LOCAL_WRITE, 1);
      }
      else
      {
        if (KAAPI_ACCESS_IS_READ(mode))
          KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_REMOTE_READ, 1);
        if (KAAPI_ACCESS_IS_WRITE(mode))
          KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_REMOTE_WRITE, 1);
      }
    }
  }
}


/*
*/
void kaapi_tracelib_task_access(
    kaapi_tracelib_thread_t*     kproc,
    kaapi_task_id_t              task,
    int                          count,
    void*                        deps,
    int                          count_noalias,
    void*                        deps_noalias,
    void                       (*decoder)(void*, int, void**, size_t*, int*)
)
{
  if ( !kaapi_perf_idset_test_mask(&kproc->perfset, MASK_PERF_READWRITE_ACCESS)
    && !(kproc->event_mask & KAAPI_EVT_MASK(KAAPI_EVT_TASK_ACCESS))
  )
    return;

#if defined(__linux__)
  int localcpu = sched_getcpu();
#if KAAPI_USE_NUMA
  int local_numaid = numa_node_of_cpu(localcpu);
#else
  int local_numaid = 0;
#endif
#else
  int local_numaid = 0;
#endif
  __kaapi_dump_access(kproc, local_numaid, task, count, deps, decoder);
  __kaapi_dump_access(kproc, local_numaid, task, count_noalias, deps_noalias, decoder);
}

/*
*/
void kaapi_tracelib_taskwait_begin(
    kaapi_tracelib_thread_t*     kproc
)
{
  KAAPI_PERFCTR_INCR(kproc, KAAPI_PERF_ID_SYNCINST, 1);
  kaapi_tracelib_thread_switchstate(kproc);
}

/*
*/
void kaapi_tracelib_taskwait_end(
    kaapi_tracelib_thread_t*     kproc
)
{
  kaapi_tracelib_thread_switchstate(kproc);
}

/*
*/
kaapi_descrformat_t* kaapi_tracelib_register_fmtdescr(
    int implicit,
    void* key,
    const char* psource,
    const char* name,
    char* (*filter_func)(char*, int, const char*, const char*)
)
{
  kaapi_hashentries_t* entry = kaapi_hashmap_find( &fdescr_map_routine, key );
  if (entry != 0)
    return KAAPI_HASHENTRIES_GET(entry, kaapi_descrformat_t*);

  kaapi_atomic_lock(&fdescr_map_lock);
  entry = kaapi_hashmap_find( &fdescr_map_routine, key );
  if (entry != 0)
  {
    kaapi_descrformat_t* pdescr =KAAPI_HASHENTRIES_GET(entry, kaapi_descrformat_t*);
    kaapi_atomic_unlock(&fdescr_map_lock);
    return pdescr;
  }

  /* kaapi_tracelib_reserve_perfcounter recopy descr in kaapi_tracelib_param.fmt_list array*/
  kaapi_descrformat_t* fdescr = kaapi_tracelib_reserve_perfcounter();
  fdescr->fmtid = (uint64_t)(uintptr_t)key;
  fdescr->implicit = implicit;
  fdescr->color = "0 0.5 1.0";
  fdescr->parent  = 0;
  fdescr->firstchild = 0; /* compute at the end from parent relation ship */
  fdescr->lastchild = 0; /* idem */
  fdescr->sibling = 0; /* idem */
  char buffer [128];
  if (name && filter_func)
  {
    filter_func(buffer, 128, psource, name);
    fdescr->name = strdup(buffer);
  }
  else if (name)
    fdescr->name = strdup(name);
  else if (psource)
    fdescr->name = strdup(psource);
  else
  {
    snprintf(buffer,128,"omp_task_%" PRIu64 "/%p", fdescr->fmtid, key );
    fdescr->name = strdup(buffer);
  }
  fdescr->perfctr->name = fdescr->name; /* to avoid */
  entry = kaapi_hashmap_insert( &fdescr_map_routine, key );
  KAAPI_HASHENTRIES_SET(entry, fdescr, kaapi_descrformat_t*);
  kaapi_atomic_unlock(&fdescr_map_lock);
  return fdescr;
}

/*
*/
kaapi_descrformat_t* kaapi_tracelib_reserve_perfcounter(void)
{
  kaapi_named_perfctr_t* perf = 0;
  kaapi_descrformat_t* retval = 0;

  perf = (kaapi_named_perfctr_t*)calloc(1,sizeof(kaapi_named_perfctr_t));
  perf->kproc_stats = (kaapi_perfstat_t**)calloc(256,sizeof(kaapi_perfstat_t*)); /* at most 256 processors ! */

  if (kaapi_tracelib_param.fmt_listsize+1 >= fmt_listcapacity)
  {
    if (fmt_listcapacity ==0) fmt_listcapacity = 2;
    kaapi_tracelib_param.fmt_list = (kaapi_descrformat_t**)realloc( kaapi_tracelib_param.fmt_list, 2*fmt_listcapacity*sizeof(kaapi_descrformat_t) );
    fmt_listcapacity *= 2;
  }

  retval = (kaapi_descrformat_t*)malloc(sizeof(kaapi_descrformat_t));
  retval->implicit = 0;
  retval->fmtid    = 0;
  retval->name     = 0;
  retval->parent   = 0;
  retval->color    = 0;
  retval->perfctr   = perf;
  kaapi_tracelib_param.fmt_list[kaapi_tracelib_param.fmt_listsize] = retval;
  ++kaapi_tracelib_param.fmt_listsize;

  kaapi_assert(retval != 0);
  return retval;
}


/*
*/
void kaapi_tracelib_barrier_begin(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     task
)
{
  KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_BARRIER, 1, task );
}


/* 
*/
void kaapi_tracelib_barrier_end(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     task
)
{
  KAAPI_EVENT_PUSH2(kproc, 0, KAAPI_EVT_BARRIER, 0, task );
}


/*
*/
void kaapi_tracelib_loop_begin(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid,
    const char*                  psource,
    uint64_t                     schedtype,
    int64_t                      lb,
    int64_t                      ub,
    int64_t                      stride,
    uint64_t                     count
)
{
  kaapi_tracelib_loop_t* loop = (kaapi_tracelib_loop_t*)malloc(sizeof(kaapi_tracelib_loop_t));
  loop->key   = (void*)wid;
  loop->name  = 0;
  loop->iter  = 0;
  loop->ub    = ub;
  loop->lb    = lb;
  loop->stride= stride;
  loop->time  = kaapi_get_elapsedns();
  loop->next  = 0;
  if (kproc->loop_tail)
    kproc->loop_tail->next = loop;
  else
    kproc->loop_head = loop;
  kproc->loop_tail = loop;
  KAAPI_EVENT_PUSH3(kproc, 0, KAAPI_EVT_LOOP_BEGIN, wid, schedtype, count );
  KAAPI_EVENT_PUSH4(kproc, 0, KAAPI_EVT_LOOP_MDATA, wid, ub, lb, stride );
}


/* 
*/
extern void kaapi_tracelib_loop_end(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid
)
{
  kaapi_tracelib_loop_t* loop = kproc->loop_tail;
  loop->time = kaapi_get_elapsedns() - loop->time;
  KAAPI_EVENT_PUSH1(kproc, 0, KAAPI_EVT_LOOP_END, wid );
}


/* 
*/
extern void kaapi_tracelib_loop_dispatch(
    kaapi_tracelib_thread_t*     kproc,
    uint64_t                     wid,
    int64_t                      lb,
    int64_t                      ub,
    int64_t                      stride
)
{
  kaapi_tracelib_loop_t* loop = kproc->loop_tail;
  int64_t size = (ub-lb)/stride+1;
  loop->iter += size;
  KAAPI_EVENT_PUSH4(kproc, 0, KAAPI_EVT_LOOP_NEXT, wid, ub, lb, stride );
}


/*
*/
static void kaapi_tracelib_thread_dumpcounter(
    kaapi_tracelib_thread_t*     kproc
)
{
  /* dump performance counter at the same date ! */
  if (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER))
  {
    int i;
    uint64_t tclock = kaapi_event_date();        
    for (i=0; i<kaapi_tracelib_thread_idsetsize(kproc); ++i)
    {
      KAAPI_EVENT_PUSH2_AT(kproc, tclock, 0,
          KAAPI_EVT_PERFCOUNTER,
          (uint64_t)(i), 
          (uint64_t)kproc->perf_regs[i]
      );
    }
  }
}


/**
*/
void _kaapi_signal_dump_counters(int xxdummy)
{
  kaapi_event_fencebuffers();
  _exit(-1);
}


/* This function should be called first to declare papi event counted before user defined counters
*/
kaapi_perf_id_t kaapi_tracelib_create_perfid( const char* name, kaapi_perf_counter_t (*read)(void*), void* ctxt )
{
  kaapi_perf_id_t id = KAAPI_PERF_ID_PAPI_BASE+user_event_count;
  if (id >= KAAPI_PERF_ID_MAX) return -1;
  kaapi_perfctr_info[id].name = strdup(name);
  kaapi_perfctr_info[id].type = KAAPI_PCTR_USER; /* except if redefined, as for PAPI */
  kaapi_perfctr_info[id].read = read;
  kaapi_perfctr_info[id].ctxt = ctxt;
  ++user_event_count;
  return id;
}


/* Used to loop over all perfcounters in set
*/
size_t kaapi_tracelib_count_perfctr(void)
{
  return KAAPI_PERF_ID_PAPI_BASE + papi_event_count + papi_uncore_event_count;
}


/* fwd decl */
static int get_event_code(char* name, int* code, uint32_t* type);

/* initialize global field kaapi_event, kaapi_name, kaapi_event_count
*/
static int kaapi_get_events(
  const char* env,
  int task_set /* 0== thread, 1== task, 2==uncore for collector */
)
{
  if ((task_set <0) || (task_set >2))
    return EINVAL;

  /* todo: [u|k]:EVENT_NAME */
  unsigned int i = 0;
  unsigned int j;
  const char* s = 0;
  char name[64];
  int event_code;

  s = getenv(env);
  if (s == NULL)
    return 0;

  if (task_set == 1)
    kaapi_perf_idset_zero( &kaapi_tracelib_param.taskperfctr_idset );

  while (*s)
  {
    uint32_t type = 0;

    for (j = 0; j < (sizeof(name) - 1) && *s && (*s != ','); ++s, ++j)
      name[j] = *s;
    name[j] = 0;

    if (get_event_code(name, &event_code, &type ) != 0)
      return -1;


    /* Register PAPI counter to be at KAAPI_PERF_ID_PAPI_BASE+cnt in kaapi_perfctr_info
    */
    if (type == KAAPI_PCTR_PAPI)
    {
      kaapi_perf_id_t newid = kaapi_tracelib_create_perfid( strdup(name), 0, 0 );
      if (newid == -1)
      {
        fprintf(stderr,"*** Kaapi: too many hardware counters defined while adding event from '%s'\n", env);
        return -1;
      }
      kaapi_perfctr_info[newid].type = KAAPI_PCTR_PAPI;
      kaapi_perfctr_info[newid].eventcode = event_code;
      switch (task_set) {
        case 1:
          kaapi_perf_idset_add( &kaapi_tracelib_param.taskperfctr_idset, newid );
        case 0:
          kaapi_perf_idset_add( &kaapi_tracelib_param.perfctr_idset, newid );
          papi_event_mask |= KAAPI_PERF_ID_MASK(newid);
          ++papi_event_count;
          if (papi_event_count >= KAAPI_MAX_HWCOUNTERS)
          {
            fprintf(stderr,"*** Kaapi: too many hardware counters defined while adding event from '%s'\n", env);
            kaapi_abort(__LINE__, __FILE__, "*** aborting");
          }
          break;
        case 2:
          kaapi_perf_idset_add( &kaapi_tracelib_param.uncoreperfctr_idset, newid );
          papi_uncore_event_mask |= KAAPI_PERF_ID_MASK(newid);
          ++papi_uncore_event_count;
          if (papi_uncore_event_count >= KAAPI_MAX_HWCOUNTERS)
          {
            fprintf(stderr,"*** Kaapi: too many hardware counters defined while adding event from '%s'\n", env);
            kaapi_abort(__LINE__, __FILE__, "*** aborting");
          }
          break;
      }
    }
    else if (type == KAAPI_PCTR_LIBRARY)
    {
      if (event_code <KAAPI_PERF_ID_MAX)
      {
        kaapi_perfctr_info[event_code].type = KAAPI_PCTR_LIBRARY;
        kaapi_perfctr_info[event_code].eventcode = event_code;
        switch (task_set) {
          case 1:
            kaapi_perf_idset_add( &kaapi_tracelib_param.taskperfctr_idset, event_code );
          case 0:
            kaapi_perf_idset_add( &kaapi_tracelib_param.perfctr_idset, event_code );
            break;
          case 2:
            kaapi_perf_idset_add( &kaapi_tracelib_param.uncoreperfctr_idset, event_code );
        }
      }
      else { /* group event */
        event_code -= KAAPI_PERF_ID_MAX;
        if (task_set)
        switch (task_set) {
          case 1:
            kaapi_perf_idset_addset( &kaapi_tracelib_param.taskperfctr_idset, kaapi_perfctr_group[event_code].mask_pertask);
          case 0:
            kaapi_perf_idset_addset( &kaapi_tracelib_param.perfctr_idset, kaapi_perfctr_group[event_code].mask);
            break;
          case 2:
            kaapi_perf_idset_addset( &kaapi_tracelib_param.uncoreperfctr_idset, kaapi_perfctr_group[event_code].mask);
        }
      }
    }
    ++i;

    if (*s == 0)
      break;

    ++s;
  }
  return 0;
}

/* Find event name '<name>' or 'KAAPI_<name>' in list of predefined events
   Return -1 if not found
*/
int get_kaapi_code( char* name )
{
  int i;
  /* group of events */
  for (i=0; i<sizeof(kaapi_perfctr_group)/sizeof(kaapi_perfctr_group_t); ++i)
  {
    if (kaapi_perfctr_group[i].name ==0)
      continue;
    if (strcasecmp(name, kaapi_perfctr_group[i].name) ==0) /* group */
      return KAAPI_PERF_ID_MAX+kaapi_perfctr_group[i].code;
    /* test KAAPI_<name> definition */
    if ((strncasecmp(name, "KAAPI_", 6) ==0)
     && (strcasecmp(name+6, kaapi_perfctr_group[i].name) ==0)) /* group */
        return KAAPI_PERF_ID_MAX+kaapi_perfctr_group[i].code;
  }

  /* specific event name ? */
  for (i=0; i<KAAPI_PERF_ID_MAX; ++i)
  {
    if (kaapi_perfctr_info[i].cmdlinename ==0) continue;
    if (strcasecmp(kaapi_perfctr_info[i].cmdlinename,name) ==0)
      return kaapi_perfctr_info[i].eventcode;
    if ((strncasecmp(name, "KAAPI_", 6) ==0)
     && (strcasecmp(kaapi_perfctr_info[i].cmdlinename,name+6) ==0))
        return kaapi_perfctr_info[i].eventcode;
  }
  return -1;
}

/* any update in this list of event => update README.envvars */
static int get_event_code(char* name, int* code, uint32_t* type)
{
  *code = -1;
  if (strncasecmp(name, "PAPI", 4) !=0)
  {
    int c = get_kaapi_code(name);
    if (c != -1)
    {
      *code = c;
      *type = KAAPI_PCTR_LIBRARY;
      return 0;
    }
    /* if name start with KAAPI_ but not known -> warning */
    if (strncasecmp(name, "KAAPI_", 5) ==0)
    {
      fprintf(stderr, "*** KAAPI bad event name:%s\n", name);
      return -1;
    }
  }
  /* else assume that its PAPI counter (included native counter) */
#if KAAPI_USE_PAPI
  int err = PAPI_event_name_to_code(name, code);
  if (err != PAPI_OK)
  {
    fprintf(stderr, "*** PAPI error code:%i\n", err);
    return -1;
  }
  *type = KAAPI_PCTR_PAPI;
  return 0;
#else
  fprintf(stderr, "*** KAAPI bad event name:%s. May be because PAPI is not configured\n", name);
  return -1;
#endif
}


/* ========================================================================= */
/* Accumulation of counters
*/
/* ========================================================================= */

static inline void update_onestate(
  kaapi_one_perfstat_t* stat,
  kaapi_perf_counter_t value
)
{
  if (stat->max < value)
    stat->max =  value;
  if (value < stat->min)
    stat->min =  value;
  stat->sum += value;
  /* pass to double for sum2 ? */
  stat->sum2 += value * value;
}


/* Update stats perf task type (==same format)
   Assume compact identification of kid
*/
void kaapi_perflib_update_perf_stat(
  uint64_t                 kid,
  int                      implicit,
  kaapi_named_perfctr_t*   perf,
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t*    counters
)
{
  unsigned int i;
  unsigned int idx;
  unsigned int count = kaapi_perf_idset_size(idset);
  kaapi_perf_idset_t set = idset;

  kaapi_perfstat_t* stats = perf->kproc_stats[kid];
  if (stats == 0)
  {
    perf->kproc_stats[kid] = stats = (kaapi_perfstat_t*)malloc( sizeof(kaapi_perfstat_t) );
    memset(perf->kproc_stats[kid], 0, sizeof(kaapi_perfstat_t) );
  }

  for (i=0; i<count; ++i)
  {
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    if (idx == KAAPI_PERF_ID_PTIME)
    {
      kaapi_assert( idset & (KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TIDLE)
                            |KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)) );
      /* if PTIME => tidle + twork defined and always at position 0, 1 ! */
      if (implicit)
        counters[i] = counters[KAAPI_PERF_ID_TIDLE];
      else
        counters[i] = counters[KAAPI_PERF_ID_WORK];
    }

    update_onestate( &stats->counters[idx], counters[i]);
  }
  ++stats->count;
}

/* Update stats perf task type (==same format)
   Assume compact identification of kid
*/
static void kaapi_perflib_accum_perf_stat(
  kaapi_perfstat_t* perf1,
  const kaapi_perfstat_t* perf2
)
{
  unsigned int i;

  for (i=0; i<KAAPI_PERF_ID_MAX; ++i)
  {
    if (perf1->counters[i].max < perf2->counters[i].max)
      perf1->counters[i].max =  perf2->counters[i].max;
    if (perf2->counters[i].min < perf1->counters[i].min)
      perf1->counters[i].min =  perf2->counters[i].min;
    perf1->counters[i].sum += perf2->counters[i].sum;
    perf1->counters[i].sum2 += perf2->counters[i].sum2;
  }
  perf1->count += perf2->count;
}


/* TODO: merge among several CPU -> parallel time to be computed !
*/
static void kaapi_mt_perf_accum_perf_stat(
  kaapi_named_perfctr_t*    perf,
  const kaapi_perf_idset_t* idset
)
{
  unsigned int i;
  unsigned int idx;
  kaapi_perfstat_t* stats = &perf->stats;
  for (i=0; i<256; ++i)
  {
    kaapi_perfstat_t* a = perf->kproc_stats[i];
    if (a ==0) continue;

    kaapi_perf_idset_t set = *idset;
    while (set !=0)
    {
      idx = __builtin_ffsl( set )-1;
      set &= ~(1UL << idx);
      if (stats->counters[idx].max < a->counters[idx].max)
        stats->counters[idx].max =  a->counters[idx].max;
      if (a->counters[idx].min < stats->counters[idx].min)
        stats->counters[idx].min =  a->counters[idx].min;
      stats->counters[idx].sum += a->counters[idx].sum;
      stats->counters[idx].sum2 += a->counters[idx].sum2;
    }
    stats->count += a->count;
  }
}


/*
*/
static void kaapi_mt_perf_cpy_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] = value[i];
}

/*
*/
static void kaapi_mt_perf_sub_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value,
  const kaapi_perf_counter_t* RESTRICT value0
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] = value[i] - value0[i];
}

/*
*/
static void kaapi_mt_perf_add_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value,
  const kaapi_perf_counter_t* RESTRICT value0
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] = value[i] + value0[i];
}

/*
*/
static void kaapi_mt_perf_subin_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] -= value[i];
}

/*
*/
static void kaapi_mt_perf_addin_counters(
  const kaapi_perf_idset_t idset,
  kaapi_perf_counter_t* RESTRICT result,
  const kaapi_perf_counter_t* RESTRICT value
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] += value[i];
}


/*
*/
static void kaapi_mt_perf_accum_threadcounters(
  kaapi_perf_idset_t set,
  kaapi_perfstat_t* stat,
  const kaapi_perf_counter_t* regs
)
{
  unsigned int i;

  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if (stat->counters[i].max < *regs)
      stat->counters[i].max =  *regs;
    if (*regs < stat->counters[i].min)
      stat->counters[i].min =  *regs;
    stat->counters[i].sum += *regs;
    stat->counters[i].sum2 += *regs * *regs;
    ++regs;
  }
  ++stat->count;
}



/*
*/
void kaapi_display_rawperf( FILE* file, const kaapi_thread_perfctr_t* counter )
{
  unsigned int j;
  for (j=0; j< kaapi_tracelib_count_perfctr(); ++j)
  {
    if (!kaapi_perf_idset_test(&kaapi_tracelib_param.perfctr_idset, j))
      continue;
    if (kaapi_perfctr_info[j].ns2s)
      fprintf(file, "%20.20s     : %e\n",
        kaapi_perfctr_info[j].name,
        1e-9*(double)(*counter)[j]
      );
    else
      fprintf(file, "%20.20s     : %" PRIi64 "\n",
        kaapi_perfctr_info[j].name,
        (*counter)[j]
      );
  }
}




/* ------------------------------------------------------------------------------------------- */
/*
*/
static void _kaapi_print_total(
  char* buffer, const char* title, int count, uint64_t gn[], double gsum[], double gsum2[]
)
{
  /* total info */
  if (count >0)
  {
    int i, pos;
    char* buff = buffer;
    pos = sprintf(buff, " %55s %10lu ", title, (unsigned long)gn[0]);
    buff += pos;
    for (i = 0; i<count; ++i)
    {
      double n = 1.0/(double)gn[i];
      double sum   = gsum[i];
      double avrg  = sum * n;
      //double avrg2 = gsum2[i];
      //double s = sqrt( avrg2 - avrg*avrg );
      //double d = 1.96 * s * sqrt(n);
      pos = sprintf(buff, "%12.4e %12.4e %12.14s ", sum, avrg, " ");
      buff += pos;
    }
  }
}

/*
*/
static void _kaapi_print_stat_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t set,
  kaapi_named_perfctr_t* perf,
  uint64_t gn[], double gsum[], double gsum2[]
)
{
  char* buff;
  int pos;
  char buffer_name[64];

  /* add indentation */
  buff = buffer_name;
  if (level >0)
  {
    int i;
    for (i=0; i<level-1; ++i)
    {
      pos = sprintf(buff,"| ");
      buff += pos;
    }
    pos = sprintf(buff,"|-");
    buff += pos;
  }
  /* recopy perf->name and replace '\n' by blanc
    sprintf(buff, "%s", perf->name);
  */
  const char* c = perf->name;
  while (*c !=0)
  {
    if (buff - buffer_name >= 63) break;
    if (*c == '\n')
      *buff = ' ';
    else
      *buff = *c;
    ++c; ++buff;
  }
  *buff = 0;

  buff = buffer;
  //printf("@buffer:%p, '%s', len=%i\n", buffer, buffer_name, (int)strlen(buffer_name));
  pos = sprintf(buff," %-55.55s %10lu ", buffer_name, perf->stats.count);
  buff += pos;
  int gidx = 0;
  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;
    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    double n = 1.0/(double)perf->stats.count;
    double sum   = c*(double)perf->stats.counters[idx].sum;
    double avrg  = sum * n;
    double avrg2 = (c*c*(double)perf->stats.counters[idx].sum2) * n;
    double s = sqrt( avrg2 - avrg*avrg );
    double d = 1.96 * s * sqrt(n);
    pos = sprintf(buff, "%12.4e %12.4e %12.4e ", sum, avrg, d);
    buff += pos;

    gn[gidx] += perf->stats.count;
    gsum[gidx] += c*(double)perf->stats.counters[idx].sum;
    gsum2[gidx] += c*c*(double)perf->stats.counters[idx].sum2;
    ++gidx;
  }
}


/* For each counter reserve space for sum/avrg/sd
*/
static void _kaapi_print_header(
  char* buffer,
  kaapi_perf_idset_t set
)
{
  char* buff = buffer;
  int pos = sprintf(buff,"#%55s %10s ", "<task name>", "<count>");
  buff += pos;

  char buffer_group [64];
  while (set !=0)
  {
    unsigned int idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    sprintf(buffer_group,"  %s (sum/avrg/sd)    ", kaapi_perfctr_info[idx].name);
    pos = sprintf(buff, "%-39.39s ", buffer_group);
    buff += pos;
  }
}


/*
*/
static void _kaapi_compute_child(void)
{
  int i;
  for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
  {
    if (kaapi_tracelib_param.fmt_list[i]->implicit ==0)
      continue;
    kaapi_descrformat_t* fdescr = kaapi_tracelib_param.fmt_list[i];
    kaapi_descrformat_t* fparent = fdescr->parent;
    if (fparent)
    {
      if (fparent->firstchild ==0)
        fparent->firstchild = fdescr;
      else
        fparent->lastchild->sibling = fdescr;
      fparent->lastchild = fdescr;
    }
  }
}


/*
*/
static void _kaapi_print_tree(
    FILE *file,
    char* buffer,
    kaapi_descrformat_t* fdescr,
    int level,
    uint64_t gn[], double gsum[], double gsum2[]
)
{
  kaapi_named_perfctr_t* perf = fdescr->perfctr;
  kaapi_mt_perf_accum_perf_stat( perf, &kaapi_tracelib_param.taskperfctr_idset);

  if (perf->stats.count !=0)
  {
    _kaapi_print_stat_set( buffer, level,
        kaapi_tracelib_param.taskperfctr_idset,
        perf, gn, gsum, gsum2
    );
    fprintf(file, "%s\n",buffer);
  }
  fdescr = fdescr->firstchild;
  while (fdescr !=0)
  {
    _kaapi_print_tree( file, buffer, fdescr, level+1, gn, gsum, gsum2 );
    fdescr = fdescr->sibling;
  }
}


/* For each counter reserve space for total/self
*/
static void _kaapi_print_team_header(
  char* buffer,
  kaapi_perf_idset_t set
)
{
  char* buff = buffer;
  int pos = sprintf(buff,"#%55s %10s %10s ", "<team name>", "<count>", "<#threads>");
  buff += pos;

  char buffer_group [64];
  while (set !=0)
  {
    unsigned int idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    sprintf(buffer_group,"| %s total self   ", kaapi_perfctr_info[idx].name);
    pos = sprintf(buff, "%-23.23s ", buffer_group);
    buff += pos;
  }
}


/*
*/
static void _kaapi_print_team_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t  set,
  kaapi_teamlinked_t* team
)
{
  char* buff;
  int pos;
  char buffer_name[64];
  kaapi_perfstat_t* self_perf = &team->self_stats;
  kaapi_perfstat_t* total_perf= &team->total_stats;

  /* add indentation */
  buff = buffer_name;
  if (level >0)
  {
    int i;
    for (i=0; i<level-1; ++i)
    {
      pos = sprintf(buff,"| ");
      buff += pos;
    }
    pos = sprintf(buff,"|-");
    buff += pos;
  }
  /* recopy team->name and replace '\n' by blanc 
    sprintf(buff, "%s", team->name);
  */
  const char* c = team->name;
  while (*c !=0)
  {
    if (buff - buffer_name >= 63) break;
    if (*c == '\n')
      *buff = ' ';
    else
      *buff = *c;
    ++c; ++buff;
  }
  *buff = 0;

  buff = buffer;
  /* */
  int i, count = 0;
  for (i=0; i<team->nthcnt; ++i)
    if ((team->thread_stats[i]) && (count < team->thread_stats[i]->count))
      count = team->thread_stats[i]->count;

  pos = sprintf(buff," %-55.55s %10i %10i ", buffer_name,
      (int)/*self_perf->*/count, (int)team->nthreads / (int)/*self_perf->*/count
  );
  buff += pos;

  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;

    double total_val;
    double self_val;

    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    /* always print sum version because nested team counters are 'sequentially accumulated' */
    if (1)//kaapi_perfctr_info[idx].opaccum ==0)
    {
      total_val = c*((double)(total_perf->counters[idx].sum));
      self_val  = c*((double)(self_perf->counters[idx].sum));
    }
    else /* a max ! */
    {
      total_val = c*((double)(total_perf->counters[idx].max));
      self_val  = c*((double)(self_perf->counters[idx].max));
    }

    pos = sprintf(buff, "%11.4e %11.4e ", total_val, self_val);
    buff += pos;
  }
}


/* Debug
*/
static void _kaapi_print_threadctr_set(
  kaapi_perf_idset_t set,
  kaapi_tracelib_thread_t* thread
)
{
  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;

    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    double total = c*((double)(thread->perf_regs[idx]));

    printf("%10.3e ", total);
  }
}


/*
*/
static int _kaapi_compute_team_tree(int nthreads, kaapi_tracelib_thread_t** threads)
{
  int i;
  kaapi_tracelib_team_t* curr;
  kaapi_hashentries_t* entry;
  kaapi_teamlinked_t* team;
  kaapi_teamlinked_t* parent_team;
  int cnt = 0;

  if (nthreads == 0)
    return 0;

  /* accumulate information about stats & create linked team object */
  for (i = 0; i<nthreads; ++i)
  {
    curr = threads[i]->head;
    while (curr !=0)
    {
      entry = kaapi_hashmap_findinsert( &team_map_routine, curr->key );
      team = KAAPI_HASHENTRIES_GET(entry, kaapi_teamlinked_t*);
#if 0
      printf("thread[%i]  team: %p, key: %p, name: %s  => %p\n", i,
             curr, curr->key, curr->name ? curr->name : "<undef>",
             (void*)team
      );
#endif
      if (team == 0)
      {
        team = (kaapi_teamlinked_t*)malloc( sizeof(kaapi_teamlinked_t) );
        team->key          = curr->key;
        team->name         = curr->name;
        team->nthreads     = 0;
        team->thread_teams = 0;
        team->nthcnt       = 0;
        team->thread_stats = 0;
        team->parent       = 0;
        team->firstchild   = 0;
        team->lastchild    = 0;
        team->sibling      = 0;
        team->next         = 0;
        memset(&team->self_stats, 0, sizeof(kaapi_perfstat_t));
        memset(&team->total_stats, 0, sizeof(kaapi_perfstat_t));
        KAAPI_HASHENTRIES_SET(entry, team, kaapi_teamlinked_t*);
        ++cnt;
        if (team_perfctr_tail ==0)
          team_perfctr_head = team;
        else
          team_perfctr_tail->next = team;
        team_perfctr_tail = team;
      }

      /* add a new threads for this team */
      team->thread_teams = (kaapi_tracelib_team_t**)realloc(team->thread_teams,
          sizeof(kaapi_tracelib_team_t*)*(team->nthreads+1)
      );
      team->thread_teams[team->nthreads] = curr;
      ++team->nthreads;
      if (team->nthcnt <= threads[i]->kid)
      {
        team->thread_stats = (kaapi_perfstat_t**)realloc(
              team->thread_stats,
              sizeof(kaapi_perfstat_t*)*(threads[i]->kid+1)
        );
        memset(&team->thread_stats[team->nthcnt], 0,
            sizeof(kaapi_perfstat_t*)*(threads[i]->kid+1-team->nthcnt)
        );
        team->nthcnt = (int)(threads[i]->kid+1);
      }
      if (team->thread_stats[threads[i]->kid] ==0)
        team->thread_stats[threads[i]->kid] = (kaapi_perfstat_t*)calloc( 1, sizeof(kaapi_perfstat_t) );
      kaapi_mt_perf_accum_threadcounters(threads[i]->perfset, team->thread_stats[threads[i]->kid], &curr->perf[0] );
      kaapi_perflib_accum_perf_stat( &team->self_stats, &curr->stats );
      kaapi_perflib_accum_perf_stat( &team->total_stats, &curr->stats );
      curr = curr->next;
    }
  }

  /* create the hierarchy of linked team */
  /* accumulate information about stats & create linked team object */
  team = team_perfctr_head;
  team_perfctr_head = team_perfctr_tail = 0;
  while (team !=0)
  {
    parent_team = 0;
    /* search for a parent: */
    for (i=0; i<team->nthreads; ++i)
    {
      if (team->thread_teams[i]->parent_team !=0)
      {
        entry = kaapi_hashmap_findinsert( &team_map_routine, team->thread_teams[i]->parent_team->key );
        parent_team = KAAPI_HASHENTRIES_GET(entry, kaapi_teamlinked_t*);
      }
    }
    if (parent_team == 0) /* its a root team */
    {
      if (team_perfctr_head ==0)
        team_perfctr_head = team;
      else
        team_perfctr_tail->sibling = team;
      team_perfctr_tail = team;
    }
    else
    {
      /* report stat */
      team->parent = parent_team;
      kaapi_perflib_accum_perf_stat( &parent_team->total_stats, &team->self_stats );
      if (parent_team->lastchild ==0)
        parent_team->firstchild = team;
      else
        parent_team->lastchild->sibling = team;
      parent_team->lastchild = team;
    }
    team = team->next;
  }

  return cnt;
}


/*
*/
static void _kaapi_print_team_tree(
    FILE *file,
    char* buffer,
    int level,
    kaapi_teamlinked_t* team
)
{
  char buffer_name[64];

  _kaapi_print_team_set( buffer, level, kaapi_tracelib_param.perfctr_idset, team );
  fprintf(file, "%s\n",buffer);

  for (int i=0; i<team->nthcnt; ++i)
  {
    if (team->thread_stats[i] ==0) continue;
    int pos;
    char* buff = buffer_name;
    /* add indentation */
    if (team->firstchild !=0)
    {
      if (level+1 >0)
      {
        int i;
        for (i=0; i<level+1; ++i)
        {
          pos = sprintf(buff,"| ");
          buff += pos;
        }
        //pos = sprintf(buff,"              |-");
        //buff += pos;
      }
    }
    *buff = 0;
    buff = buffer;
    pos = sprintf(buff," %-55.55s %17s%4i ", buffer_name, "thread", i );
    buff += pos;

    kaapi_perf_idset_t set = kaapi_tracelib_param.perfctr_idset;
    while (set !=0)
    {
      unsigned int idx;
      idx = __builtin_ffsl( set )-1;
      set &= ~(1UL << idx);
      double c;

      double self_val;

      /* scale PERF_ID_TIME counter to be in second */
      if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
      else c = 1.0;
      /* always print sum version because nested team counters are 'sequentially accumulated' */
      if (1)//kaapi_perfctr_info[idx].opaccum ==0)
        self_val  = c*((double)(team->thread_stats[i]->counters[idx].sum));
      else /* a max ! */
        self_val  = c*((double)(team->thread_stats[i]->counters[idx].max));

      pos = sprintf(buff, "%11s %11.4e ", "", self_val);
      buff += pos;
    }
    fprintf(file, "%s\n",buffer);
  }

  kaapi_teamlinked_t* child = team->firstchild;
  while (child !=0)
  {
    _kaapi_print_team_tree( file, buffer, level+1, child );
    child = child->sibling;
  }
}


#if LIBOMP_USE_NUMA
/* Each uncore thread is attached to a socket (i.e. to a core to a socket).
*/
void* _kaapi_uncore_collector(void* arg )
{
  if (kaapi_tracelib_param.uncoreperfctr_idset ==0 )
    return 0;
  /* force to run on given socket */
  int numaid = (int)(intptr_t)arg;
  int cpu;
  int err = numa_run_on_node(numaid);
  if (err) fprintf(stderr,"numa_run_on_node(%i): error '%s'\n", numaid, strerror(errno));
  while (1)
  {
    cpu = sched_getcpu();
    int node = numa_node_of_cpu(cpu);
    if (node == numaid)
      break;
    sched_yield();
  }
  /* signal for master thread */
  KAAPI_ATOMIC_DECR(&kaapi_uncore_thread_counter);

  kaapi_tracelib_thread_t* kproc = kaapi_tracelib_thread_init(-1, cpu, -1, 2 /* to be uncore collector */ );
  kaapi_perf_counter_t regs[KAAPI_PERF_ID_MAX];

  /* infinite loop */
  struct timespec delay = { kaapi_tracelib_param.uncore_period/1000000000ULL, kaapi_tracelib_param.uncore_period%1000000000 };
  kaapi_tracelib_thread_start(kproc);
  while (KAAPI_ATOMIC_READ(&kaapi_uncore_thread_finish) == 0)
  {
    kaapi_tracelib_thread_read(kproc, kproc->perfset, regs);
    KAAPI_EVENT_PUSH_UNCORE_PERFCTR( kproc, 0, numaid, &kproc->perfset, regs );
#if __linux__
    clock_nanosleep(CLOCK_REALTIME, 0, &delay, 0 );
#else
    struct timespec ts = delay;
    struct timespec rem;
    clock_gettime( CLOCK_REALTIME, &ts );
    ts.tv_nsec += delay.tv_nsec;
    ts.tv_sec += delay.tv_sec;
    if (ts.tv_nsec >= 1000000000ULL)
    {
      ts.tv_sec = ts.tv_nsec / 1000000000ULL;
      ts.tv_nsec = ts.tv_nsec % 1000000000ULL;
    }
    while (nanosleep( &ts, &rem) ==EINTR)
    {
      ts = rem;
    }
#endif
  }
  kaapi_tracelib_thread_stop(kproc);
  kaapi_tracelib_thread_fini(kproc);

  return 0;
}
#endif //NUMA

#if defined(__cplusplus)
}
#endif
